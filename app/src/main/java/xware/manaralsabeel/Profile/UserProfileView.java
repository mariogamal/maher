package xware.manaralsabeel.Profile;

import xware.manaralsabeel.Base.BaseView;

public interface UserProfileView extends BaseView {

    void loadStudentDetails(Student student);

    void updateMenu();

}
