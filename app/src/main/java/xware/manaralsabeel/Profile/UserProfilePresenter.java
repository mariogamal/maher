package xware.manaralsabeel.Profile;

import android.util.Log;
import java.io.File;
import java.util.HashMap;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BuildConfig;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class UserProfilePresenter extends BasePresenter<UserProfileView> {

    public void updateProfile(final Student student){
        getView().showProgress();
        APIManager.getInstance().getAPI().updateStudent(
                BaseApplication.preferences.getString("id",""),student).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                getView().hideProgress();
                updateStudentData(response.body());
                getView().showMessage(" تم تحديث البيانات");
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    public void getStudentDetails(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getStudent(
                BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                getView().loadStudentDetails(response.body());
                updateStudentData(response.body());
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    private void updateStudentData(Student student){
        if (student.getFirstName() == null)
            student.setFirstName("");
        if (student.getLastName() == null)
            student.setLastName("");
        BaseApplication.editor.putInt("points", student.getPoints());
        BaseApplication.editor.putString("idmUuid", student.getIdmUuid());
        BaseApplication.editor.putString("name", student.getFullName());
        BaseApplication.editor.putBoolean("isComplete", student.isComplete());
        if (student.getPictureUrl() != null)
            BaseApplication.editor.putString("photo", student.getPictureUrl());
        else
            BaseApplication.editor.putString("photo", "");

        BaseApplication.editor.commit();
        getView().updateMenu();
    }

    public void uploadPhoto(File file, final Student student){
        getView().showProgress();
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", "image", reqFile);
        final retrofit2.Call<HashMap<String,String>> req = APIManager.getUniqueInstance(BuildConfig.baseStrg).getAPI().postImage(body);
        req.enqueue(new Callback<HashMap<String,String>>() {
            @Override
            public void onResponse(Call<HashMap<String,String>> call, Response<HashMap<String,String>> response) {
                if (response.code()== 201) {
                    HashMap<String, String> url = response.body();
                    student.pictureUrl = BuildConfig.baseStrg.concat(url.get("url"));
                    updateProfile(student);
                }
            }

            @Override
            public void onFailure(Call<HashMap<String,String>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                t.printStackTrace();
            }
        });
    }

}
