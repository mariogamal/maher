package xware.manaralsabeel.Profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class UserProfile extends BaseFragment implements UserProfileView {

    EditText firstName, lastName, email;
    Spinner nationality, gender;
    TextView phone, linkFacebook, linkTwitter;
    File file;
    CircleImageView picture;
    String[] nationalities, genders;
    UserProfilePresenter profilePresenter;
    boolean photoSelected;

    //--------------
    private FirebaseAuth mAuth;
    private LoginManager mLoginMgr;
    private CallbackManager mCallbackManager;

    public UserProfile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_profile, container, false);
        Button save = v.findViewById(R.id.save);
        firstName = v.findViewById(R.id.firstname);
        lastName = v.findViewById(R.id.lastname);
        email = v.findViewById(R.id.email);
        phone = v.findViewById(R.id.phone);
        picture = v.findViewById(R.id.picture);
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, ""), 0);
                }
            }
        });
        photoSelected = false;
        initSpinners(v);
        profilePresenter = new UserProfilePresenter();
        profilePresenter.attachView(this);
        profilePresenter.getStudentDetails();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidUser(getStudent())) {
                    if (photoSelected)
                        profilePresenter.uploadPhoto(file, getStudent());
                    else
                        profilePresenter.updateProfile(getStudent());
                }
            }
        });

        linkFacebook = v.findViewById(R.id.linkFacebook);
        linkTwitter = v.findViewById(R.id.linkTwitter);
        initSocialLinking();
        linkFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!linkFacebook.getText().equals(getResources().getString(R.string.linked)))
                    initFacebookLogin();
//                else
//                    showMessage("الحساب مرتبط بالفعل");
            }
        });


        linkTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!linkTwitter.getText().equals(getResources().getString(R.string.linked)))
                    initTwitterLogin();
//                else
//                    showMessage("الحساب مرتبط بالفعل");
            }
        });


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((NavMenu) getActivity()).setTitleText(getResources()
                .getString(R.string.profile));
    }

    private void initSocialLinking() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        for (UserInfo profile : user.getProviderData()) {
            String providerId = profile.getProviderId();
            switch (providerId) {
                case "facebook.com":
                    linkFacebook.setText(R.string.linked);
                    break;

                case "twitter.com":
                    linkTwitter.setText(R.string.linked);
                    break;
            }
        }
    }

    private void initTwitterLogin() {
    }

    private void initFacebookLogin() {
        mCallbackManager = CallbackManager.Factory.create();
        mLoginMgr = LoginManager.getInstance();
        mLoginMgr.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken token = loginResult.getAccessToken();
                AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
                linkAuthProvider(credential);
                linkFacebook.setText(R.string.linked);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
        mLoginMgr.logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
    }

    private void linkAuthProvider(AuthCredential credential) {
        mAuth = FirebaseAuth.getInstance();
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                        } else {

                        }
                    }
                });
    }

    private void initSpinners(View v) {
        nationality = v.findViewById(R.id.nationality);
        gender = v.findViewById(R.id.gender);

        nationalities = getResources().getStringArray(R.array.nationality);
        genders = getResources().getStringArray(R.array.gender);

        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_layout, nationalities);
        nationality.setAdapter(nationalityAdapter);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_layout, genders);
        gender.setAdapter(genderAdapter);
    }

    @Override
    public void loadStudentDetails(Student student) {

        Picasso.get().load(student.getPictureUrl())
                .placeholder(R.drawable.profileicon)
                .error(R.drawable.profileicon)
                .into(picture);
        picture.setTag(student.getPictureUrl());

        phone.setText(student.getPhone());
        firstName.setText(student.getFirstName());
        lastName.setText(student.getLastName());
        email.setText(student.getEmail());

        if (student.getGender() != null) {
            if (!student.getGender().equals("Male"))
                gender.setSelection(1);
        }

        if (student.getNationality() != null) {
            switch (student.getNationality()) {
                case "سعودى":
                    nationality.setSelection(0);
                    break;

                case "مصرى":
                    nationality.setSelection(1);
                    break;

                case "اردنى":
                    nationality.setSelection(2);
                    break;
            }
        }

        hideProgress();
    }

    @Override
    public void updateMenu() {
        ((NavMenu) getActivity()).updateDrawerHeader();
    }


    public Student getStudent() {
        Student student = new Student();
        student.setFirstName(firstName.getText().toString());
        student.setLastName(lastName.getText().toString());
        student.setEmail(email.getText().toString());
        student.setPhone(BaseApplication.preferences.getString("phone", ""));
        student.setNationality(nationality.getSelectedItem().toString());
        student.setGender(gender.getSelectedItem().toString().equals("ذكر") ? "Male" : "Female");
        student.setPictureUrl((String) picture.getTag());
        return student;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            photoSelected = true;
            android.net.Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            android.database.Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null)
                return;

            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            file = new File(filePath);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            picture.setImageBitmap(bitmap);
        } else {
            if (mCallbackManager != null)
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, ""), 0);
            } else
                showMessage(getString(R.string.storage_permission_required));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        profilePresenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }

    private boolean isValidUser(Student user) {
        if (TextUtils.isEmpty(user.getFirstName())) {
            showMessage(getString(R.string.enter_first_name));
            return false;
        } else if (TextUtils.isEmpty(user.getLastName())) {
            showMessage(getString(R.string.enter_last_name));
            return false;
        } else if (TextUtils.isEmpty(user.getEmail()) || !android.util.Patterns.EMAIL_ADDRESS.matcher(user.getEmail()).matches()) {
            showMessage(getString(R.string.enter_email));
            return false;
        } else if (!photoSelected && TextUtils.isEmpty(user.getPictureUrl())) {
            showMessage(getString(R.string.choose_profile_picture));
            return false;
        } else
            return true;
    }
}
