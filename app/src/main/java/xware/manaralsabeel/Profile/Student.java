package xware.manaralsabeel.Profile;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class Student {

    @SerializedName("id")
    public Integer id;

    @SerializedName("isActive")
    public Boolean isActive;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("lastName")
    public String lastName;

    @SerializedName("gender")
    public String gender;

    @SerializedName("email")
    public String email;

    @SerializedName("pictureUrl")
    public String pictureUrl;

    @SerializedName("points")
    public Integer points;

    @SerializedName("nationality")
    public String nationality;

    @SerializedName("facebookUrl")
    public String facebookUrl;

    @SerializedName("twitterUrl")
    public String twitterUrl;

    @SerializedName("isRegistered")
    public Boolean isRegistered;

    @SerializedName("phone")
    public String phone;

    @SerializedName("idmUuid")
    public String idmUuid;

    public Student() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public Boolean getRegistered() {
        return isRegistered;
    }

    public void setRegistered(Boolean registered) {
        isRegistered = registered;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName(){
        return firstName+" "+lastName;
    }

    public String getIdmUuid() {
        return idmUuid;
    }

    public void setIdmUuid(String idmUuid) {
        this.idmUuid = idmUuid;
    }

    public boolean isComplete(){
        return !TextUtils.isEmpty(firstName)
                && !TextUtils.isEmpty(lastName)
                && !TextUtils.isEmpty(email)
                && !TextUtils.isEmpty(gender)
                && !TextUtils.isEmpty(nationality)
                && !TextUtils.isEmpty(pictureUrl);
    }
}
