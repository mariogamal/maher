package xware.manaralsabeel.BrowseSubjects;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BrowseSubjects.Levels.PrepLevel.PrepFragment;
import xware.manaralsabeel.BrowseSubjects.Levels.PrimaryLevel.PrimaryFragment;
import xware.manaralsabeel.BrowseSubjects.Levels.SecondaryLevel.SecondaryFragment;

public class LevelsPagerAdapter extends FragmentPagerAdapter {
    
    public LevelsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new PrimaryFragment();

            case 1:
                return new PrepFragment();

            case 2:
                return new SecondaryFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return BaseApplication.levels.size();
    }
}
