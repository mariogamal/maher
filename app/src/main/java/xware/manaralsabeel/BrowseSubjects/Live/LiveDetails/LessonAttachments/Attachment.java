package xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments;

import com.google.gson.annotations.SerializedName;

public class Attachment {
    @SerializedName("type")
    private Object type;

    @SerializedName("lesson")
    private Integer lesson;

    @SerializedName("url")
    private String url;

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Integer getLesson() {
        return lesson;
    }

    public void setLesson(Integer lesson) {
        this.lesson = lesson;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
