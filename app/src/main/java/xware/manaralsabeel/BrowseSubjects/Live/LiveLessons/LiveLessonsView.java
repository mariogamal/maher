package xware.manaralsabeel.BrowseSubjects.Live.LiveLessons;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface LiveLessonsView extends BaseView {

    void displayLessons(List<LiveLesson> lessons);
}
