package xware.manaralsabeel.BrowseSubjects.Live.LiveLessons;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LiveDetailFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class LiveLessonsFragment extends BaseFragment implements LiveLessonsView {


    TextView subjectName;
    ImageView subjectIcon;
    LinearLayout lessonsParent;
    LiveLessonsPresenter presenter;
    public LiveLessonsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_lessons, container, false);
        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.live_lessons));
        subjectName = view.findViewById(R.id.subjectname);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = view.findViewById(R.id.subjecticon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        lessonsParent = view.findViewById(R.id.lessonsParent);
        presenter = new LiveLessonsPresenter();
        presenter.attachView(this);
        presenter.loadLessons();
        return view;
    }

    @Override
    public void displayLessons(List<LiveLesson> lessons) {
        for (final LiveLesson lesson : lessons){
            View lessonLayout = getActivity().getLayoutInflater().inflate(R.layout.live_item_layout,null);

            TextView teacherName = lessonLayout.findViewById(R.id.teacher);
            teacherName.setText(lesson.getTeacherSubject().getTeacher().getName());

            TextView title = lessonLayout.findViewById(R.id.title);
            title.setText(lesson.getTitle());

            String startDate = lesson.getStartDateTime().substring(0,10);
            String startTime = lesson.getStartDateTime().substring(11,16);

            TextView date = lessonLayout.findViewById(R.id.date);
            date.setText(startDate);

            TextView time = lessonLayout.findViewById(R.id.time);
            time.setText(getString(R.string.clock).concat(startTime));
            BaseApplication.startTime = getString(R.string.clock).concat(startTime);

            TextView duration = lessonLayout.findViewById(R.id.duration);
            duration.setText(getResources().getString((R.string.duration)).concat(" ")
                    .concat(String.valueOf(lesson.getDuration())).concat(" ")
                    .concat(getString(R.string.minutes)));

            DateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            DateFormat dateFormatter = new SimpleDateFormat("EEEE dd MMMM yyyy",new Locale("ar"));
            try {
                Date parsed = dateParser.parse(startDate);
                String formatted = dateFormatter.format(parsed);
                date.setText(formatted);
                BaseApplication.startDate = formatted;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }


            LinearLayout cellLayout = lessonLayout.findViewById(R.id.cell_layout);

            cellLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BaseApplication.lessonID = lesson.getId();
                    BaseApplication.liveLesson = lesson;
                    BaseApplication.TeacherID = lesson.getTeacherSubject().getTeacher().getUuid();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content, new LiveDetailFragment())
                            .addToBackStack(null)
                            .commit();


                }
            });
            lessonsParent.addView(lessonLayout);
        }
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }

}
