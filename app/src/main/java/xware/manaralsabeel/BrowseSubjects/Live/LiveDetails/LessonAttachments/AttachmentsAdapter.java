package xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import xware.manaralsabeel.R;

public class AttachmentsAdapter extends BaseAdapter {

    private List<Attachment> attachments;
    private Context context;

    public AttachmentsAdapter(List<Attachment> attachments, Context context) {
        this.attachments = attachments;
        this.context = context;
    }

    @Override
    public int getCount() {
        return attachments.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.attached_files_item,null);
            holder = new ViewHolder();
            holder.title = convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        holder.title.setText(attachments.get(position).getName());
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(attachments.get(position).getUrl()));
                    context.startActivity(i);
                }
                catch (Exception ignore){}
            }
        });
        return convertView;
    }

    class ViewHolder{
        TextView title;
    }
}
