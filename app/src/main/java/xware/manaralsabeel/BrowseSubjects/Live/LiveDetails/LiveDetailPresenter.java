package xware.manaralsabeel.BrowseSubjects.Live.LiveDetails;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments.Attachment;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class LiveDetailPresenter extends BasePresenter<LiveDetailView> {
    public void loadAttachments(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getLessonAttachments(
                BaseApplication.lessonID).enqueue(new Callback<List<Attachment>>() {
            @Override
            public void onResponse(Call<List<Attachment>> call, Response<List<Attachment>> response) {
                getView().showLessons(response.body());
            }

            @Override
            public void onFailure(Call<List<Attachment>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }
}
