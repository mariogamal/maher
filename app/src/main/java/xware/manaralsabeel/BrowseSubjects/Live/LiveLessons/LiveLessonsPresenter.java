package xware.manaralsabeel.BrowseSubjects.Live.LiveLessons;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class LiveLessonsPresenter extends BasePresenter<LiveLessonsView> {
    public void loadLessons() {
        getView().showProgress();
        APIManager.getInstance().getAPI().getLiveLessons(
                BaseApplication.subjectID).enqueue(new Callback<List<LiveLesson>>() {
            @Override
            public void onResponse(Call<List<LiveLesson>> call, Response<List<LiveLesson>> response) {
                getView().displayLessons(getActiveLessons(response.body()));
            }

            @Override
            public void onFailure(Call<List<LiveLesson>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }

    private List<LiveLesson> getActiveLessons (List<LiveLesson> lessons){
        List<LiveLesson> activeLessons = new ArrayList<>();
        for (LiveLesson lesson : lessons){
            if (lesson.getActive())
                activeLessons.add(lesson);
        }
        return activeLessons;
    }
}
