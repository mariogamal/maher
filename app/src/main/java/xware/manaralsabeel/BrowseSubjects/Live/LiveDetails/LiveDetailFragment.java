package xware.manaralsabeel.BrowseSubjects.Live.LiveDetails;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;
import java.util.List;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments.Attachment;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments.AttachmentsAdapter;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class LiveDetailFragment extends BaseFragment implements LiveDetailView, YouTubePlayer.OnInitializedListener {

    TextView subjectName;
    ImageView subjectIcon, fullScreen;
    TextView title, date, time, noAttached;
    ListView attachmentList;
    LiveDetailPresenter presenter;

    public LiveDetailFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_detail, container, false);
        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.live_lessons));
        subjectName = view.findViewById(R.id.subjectname);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = view.findViewById(R.id.subjecticon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        attachmentList = view.findViewById(R.id.attachmentsList);
        noAttached = view.findViewById(R.id.attached);
        fullScreen = view.findViewById(R.id.full_screen);
        title = view.findViewById(R.id.title);
        date = view.findViewById(R.id.date);
        time = view.findViewById(R.id.time);

        title.setText(BaseApplication.liveLesson.getTitle());
        date.setText(BaseApplication.startDate);
        time.setText(BaseApplication.startTime);
        fullScreen.setVisibility(View.GONE);

        YouTubePlayerSupportFragment youtubePlayerFragment = new YouTubePlayerSupportFragment();
        youtubePlayerFragment.initialize(getString(R.string.api_key), this);
        getFragmentManager().beginTransaction().replace(R.id.liveVideo,youtubePlayerFragment).commit();

        presenter = new LiveDetailPresenter();
        presenter.attachView(this);
        presenter.loadAttachments();

        return view;
    }

    @Override
    public void showLessons(List<Attachment> attachments) {
        if (attachments.size() == 0)
        {
            noAttached.setVisibility(View.VISIBLE);
            attachmentList.setVisibility(View.INVISIBLE);
        }
        else
        {
            noAttached.setVisibility(View.INVISIBLE);
            attachmentList.setVisibility(View.VISIBLE);
            AttachmentsAdapter adapter = new AttachmentsAdapter(attachments,getActivity());
            attachmentList.setAdapter(adapter);
        }
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
        String url = BaseApplication.liveLesson.getUrl();
        if (url != null){
            String key = url.substring(url.indexOf("=")+1);
            Log.d("key",key);
            if (!b) {
                youTubePlayer.cueVideo(key);
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                fullScreen.setVisibility(View.VISIBLE);
                fullScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        youTubePlayer.setFullscreen(true);
                    }
                });
            }
        }
        else
            showMessage(getString(R.string.cant_stream_now));
        final NavMenu activity = (NavMenu) getActivity();
        activity.youTubePlayer = youTubePlayer;
        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                activity.isYoutubePlayerFullScren = b;
            }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
