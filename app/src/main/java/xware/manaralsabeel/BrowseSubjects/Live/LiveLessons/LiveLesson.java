package xware.manaralsabeel.BrowseSubjects.Live.LiveLessons;

import com.google.gson.annotations.SerializedName;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons.TeacherSubject;

public class LiveLesson {
    @SerializedName("id")
    private Integer id;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    @SerializedName("duration")
    private Integer duration;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("teacherSubject")
    private TeacherSubject teacherSubject;

    @SerializedName("startDateTime")
    private String startDateTime;

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public TeacherSubject getTeacherSubject() {
        return teacherSubject;
    }

    public void setTeacherSubject(TeacherSubject teacherSubject) {
        this.teacherSubject = teacherSubject;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
