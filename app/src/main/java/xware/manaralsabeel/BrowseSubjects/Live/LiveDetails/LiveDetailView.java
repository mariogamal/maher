package xware.manaralsabeel.BrowseSubjects.Live.LiveDetails;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments.Attachment;

public interface LiveDetailView extends BaseView {

    void showLessons(List<Attachment> attachments);
}
