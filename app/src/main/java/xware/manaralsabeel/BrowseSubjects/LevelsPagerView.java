package xware.manaralsabeel.BrowseSubjects;

import java.util.List;
import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Levels.Level;

public interface LevelsPagerView extends BaseView{

    void setTitles(List<Level> levels);

}
