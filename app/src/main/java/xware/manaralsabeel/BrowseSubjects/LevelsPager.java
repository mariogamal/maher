package xware.manaralsabeel.BrowseSubjects;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BrowseSubjects.Levels.Level;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.Dialogs.ProgressDialog;

public class LevelsPager extends Fragment implements LevelsPagerView {

    TabLayout tabLayout;
    ViewPager viewPager;
    LevelsPagerAdapter levelsPagerAdapter;
    LevelsPagerPresenter presenter;
    Typeface cairoBold;
    Typeface cairoRegular;
    Typeface cairoSemiBold;

    public LevelsPager() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_levels_pager, container, false);
        ((NavMenu) getActivity()).setTitleText(getResources().getString(R.string.browse_subjects));
        presenter = new LevelsPagerPresenter();
        presenter.attachView(this);
        presenter.getLevels();

        cairoRegular = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.cairo_regular_font));
        cairoSemiBold = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.cairo_semibold_font));

        viewPager = (ViewPager) v.findViewById(R.id.levelsPager);

        tabLayout = (TabLayout) v.findViewById(R.id.levelsTabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LinearLayout tabs = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabs.getChildAt(1);
                tabTextView.setTypeface(cairoSemiBold);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                LinearLayout tabs = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(tab.getPosition());
                TextView tabTextView = (TextView) tabs.getChildAt(1);
                tabTextView.setTypeface(cairoRegular);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onTabSelected(tab);
            }
        });
        return v;
    }

    @Override
    public void showProgress() {
        ProgressDialog.getInstance().show(getActivity());
    }

    @Override
    public void hideProgress() {
        ProgressDialog.getInstance().dismiss();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setTitles(List<Level> levels) {

        levelsPagerAdapter = new LevelsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(levelsPagerAdapter);

        for (int i = 0; i < levels.size(); i++) {
            if (levels.get(i).getName() != null)
                tabLayout.getTabAt(i).setText(levels.get(i).getName());
        }

        TabLayout.Tab tab = tabLayout.getTabAt(0);
        tab.select();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
