package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface ExamQuestionsView extends BaseView {
    void displaySubjectExam(List<Question> questions);
}
