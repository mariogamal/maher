package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import xware.manaralsabeel.R;

public class ExamView {

    private Context context;
    private int questionIndex;
    boolean examResult[];
    Button nextQuestion;
    List<Question> questions;

    ExamView(View view, Context context) {
        nextQuestion = view.findViewById(R.id.next_question);
        this.context = context;
        questionIndex = 0;
    }

    void showNextButton(){
        nextQuestion.setVisibility(View.VISIBLE);
    }

    void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    void setExamResult(boolean[] examResult) {
        this.examResult = examResult;
    }

    int getQuestionIndex() {
        return questionIndex;
    }

    void nextQuestion() {
        questionIndex++;
    }

    void setNextQuestionClickListener(QuestionView questionView, ExamQuestionsFragment fragment) {
        nextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (questionView.answerChecked)
                    fragment.getNextQuestion();
                else
                    Toast.makeText(context, context.getString
                            (R.string.please_choose_answer), Toast.LENGTH_SHORT).show();
            }
        });
    }
}