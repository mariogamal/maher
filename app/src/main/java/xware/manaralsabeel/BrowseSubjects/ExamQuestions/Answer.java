package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("id")
    private Integer id;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("choice")
    private String choice;

    @SerializedName("isTrue")
    private Boolean isTrue;

    @SerializedName("question")
    private Integer question;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public Boolean getCorrect() {
        return isTrue;
    }

    public void setTrue(Boolean aTrue) {
        isTrue = aTrue;
    }

    public Integer getQuestion() {
        return question;
    }

    public void setQuestion(Integer question) {
        this.question = question;
    }
}
