package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.Dialogs.ImageZoomDialog;

public class QuestionView {

    Context context;
    boolean answerChecked;
    TextView questionText;
    private TextView questionNumber;
    ImageView questionImage;
    RadioGroup questionAnswers;
    RadioButton answerChoices[];

    QuestionView(View view, Context context) {
        this.context = context;
        questionText = view.findViewById(R.id.question);
        questionImage = view.findViewById(R.id.question_image);
        questionAnswers = view.findViewById(R.id.question_answers);
        questionNumber = view.findViewById(R.id.question_number);
    }

    void initAnswers(int count) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.cairo_regular_font));
        answerChoices = new RadioButton[count];
        for (int i = 0; i < count; i++) {
            answerChoices[i] = new RadioButton(context);
            answerChoices[i].setTextColor(context.getResources().getColor(R.color.navy));
            answerChoices[i].setTypeface(font);
            answerChoices[i].setTextSize(context.getResources().getDimension(R.dimen._4ssp));
            questionAnswers.addView(answerChoices[i]);
        }
    }

    void setImageClickListener(ExamView examView) {
        questionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageZoomDialog.getInstance().show(context,
                        examView.questions.get(examView.getQuestionIndex()).getPicURL());
            }
        });
    }

    void setRadioCheckChangeListener(ExamView examView, View view) {
        questionAnswers.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton checkedRadioButton = view.findViewById(checkedId);
                if (checkedRadioButton != null) {
                    answerChecked = true;
                    examView.examResult[examView.getQuestionIndex()] = (boolean) checkedRadioButton.getTag();
                } else
                    answerChecked = false;
            }
        });
    }

    void setQuestionNumberText(String text){
        questionNumber.setText(text);
    }
}
