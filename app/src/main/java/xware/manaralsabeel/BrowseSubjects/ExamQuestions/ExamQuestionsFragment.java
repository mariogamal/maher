package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.Dialogs.ExamResultDialog;

public class ExamQuestionsFragment extends BaseFragment implements ExamQuestionsView {

    QuestionView questionView;
    ExamView examView;

    public ExamQuestionsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.exams));
        View view = inflater.inflate(R.layout.fragment_subject_question, container, false);
        questionView = new QuestionView(view, getActivity());
        examView = new ExamView(view, getActivity());
        questionView.setImageClickListener(examView);
        examView.setNextQuestionClickListener(questionView, this);
        questionView.setRadioCheckChangeListener(examView, view);

        TextView subjectTitle = view.findViewById(R.id.subject_title);
        subjectTitle.setText(BaseApplication.subject.getName());
        ImageView subjectIcon = view.findViewById(R.id.subject_icon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        ExamQuestionsPresenter presenter = new ExamQuestionsPresenter();
        presenter.attachView(this);
        presenter.getQuestions();
        return view;
    }

    private void fetchQuestion() {
        String[] questionTitles = getResources().getStringArray(R.array.questions);
        questionView.setQuestionNumberText(questionTitles[examView.getQuestionIndex()]);
        Question question = examView.questions.get(examView.getQuestionIndex());
        questionView.questionText.setText(question.getBody());
        Picasso.get().load(question.getPicURL()).into(questionView.questionImage);
        for (int i = 0; i < questionView.answerChoices.length; i++) {
            questionView.answerChoices[i].setText(question.getAnswers().get(i).getChoice());
            questionView.answerChoices[i].setTag(question.getAnswers().get(i).getCorrect());
        }
    }

    @Override
    public void displaySubjectExam(List<Question> questions) {
        if (questions.size() == 0)
            showMessage(getString(R.string.no_questions));
        else {
            examView.showNextButton();
            examView.setQuestions(questions);
            examView.setExamResult(new boolean[questions.size()]);
            initQuestionAnswers(questions.get(0).getAnswers().size());
        }
    }

    private void initQuestionAnswers(int answersCount) {
        questionView.initAnswers(answersCount);
        fetchQuestion();
    }

    void getNextQuestion() {
        questionView.questionAnswers.clearCheck();
        examView.nextQuestion();
        handleTextChange();
        if (examView.getQuestionIndex() == examView.questions.size())
            displayResult();
        else
            fetchQuestion();
    }

    private void handleTextChange() {
        if (examView.getQuestionIndex() == examView.questions.size() - 1)
            examView.nextQuestion.setText(R.string.finish);
    }

    private void displayResult() {
        int count = 0;
        for (boolean answer : examView.examResult) {
            if (answer)
                count++;
        }
        ExamResultDialog.getInstance().show(getActivity(), count, examView.questions.size());
    }
}
