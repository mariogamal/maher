package xware.manaralsabeel.BrowseSubjects.ExamQuestions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;
import xware.manaralsabeel.R;

public class ExamQuestionsPresenter extends BasePresenter<ExamQuestionsView> {

    public void getQuestions() {
        getView().showProgress();
        if (BaseApplication.isLessonQuestions)
            getLessonQuestions();
        else
            getSubjectQuestions();
    }

    private void getSubjectQuestions() {
        APIManager.getInstance().getAPI().getSubjectQuestions(BaseApplication.subjectID)
                .enqueue(new Callback<List<Question>>() {
                    @Override
                    public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                        onSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Question>> call, Throwable t) {
                        onError();
                    }
                });
    }

    private void getLessonQuestions() {
        APIManager.getInstance().getAPI().getLessonQuestions(
                BaseApplication.lessonID)
                .enqueue(new Callback<List<Question>>() {
                    @Override
                    public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                        onSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Question>> call, Throwable t) {
                        onError();
                    }
                });
    }

    private void onSuccess(List<Question> questions) {
        getView().hideProgress();
        getView().displaySubjectExam(getActiveQuestions(questions));
    }

    private void onError() {
        getView().hideProgress();
        getView().showMessage(BaseApplication.error_msg);
    }

    private List<Question> getActiveQuestions(List<Question> questions) {
        List<Question> activeQuestions = new ArrayList<>();
        for (Question question : questions) {
            if (question.getActive())
                activeQuestions.add(question);
        }
        return activeQuestions;
    }
}
