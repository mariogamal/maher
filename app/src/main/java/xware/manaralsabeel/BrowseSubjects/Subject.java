package xware.manaralsabeel.BrowseSubjects;

import com.google.gson.annotations.SerializedName;
import xware.manaralsabeel.MySubjects.Grade;

public class Subject {

    @SerializedName("id")
    private Integer id;

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("name")
    private String name;

    @SerializedName("grade")
    private Grade grade;

    public Subject() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }
}
