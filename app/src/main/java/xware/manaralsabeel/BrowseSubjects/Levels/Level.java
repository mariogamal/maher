package xware.manaralsabeel.BrowseSubjects.Levels;

import com.google.gson.annotations.SerializedName;

public class Level {

    @SerializedName("id")
    private Integer id;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("name")
    private String name;

    public Level() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
