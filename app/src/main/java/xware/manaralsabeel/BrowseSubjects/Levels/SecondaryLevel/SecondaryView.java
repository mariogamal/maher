package xware.manaralsabeel.BrowseSubjects.Levels.SecondaryLevel;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Levels.Grade;
import xware.manaralsabeel.BrowseSubjects.Subject;

public interface SecondaryView extends BaseView {

    void loadSubjects(Grade grade, List<Subject> subjects, int position);

    void setGradesCount(int count);
}
