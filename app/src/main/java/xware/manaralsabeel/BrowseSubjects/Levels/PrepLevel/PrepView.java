package xware.manaralsabeel.BrowseSubjects.Levels.PrepLevel;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Levels.Grade;
import xware.manaralsabeel.BrowseSubjects.Subject;

public interface PrepView extends BaseView {

    void loadSubjects(Grade grade, List<Subject> subjects, int position);

    void setGradesCount(int count);
}
