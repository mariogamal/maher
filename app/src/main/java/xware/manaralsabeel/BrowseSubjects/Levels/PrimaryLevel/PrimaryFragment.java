package xware.manaralsabeel.BrowseSubjects.Levels.PrimaryLevel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.Levels.Grade;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.R;

public class PrimaryFragment extends BaseFragment implements PrimaryView {

    PrimaryPresenter primaryPresenter;
    LinearLayout levelsParent;
    View [] gradesViews;
    int GradesCount;
    int calledTimes;
    public PrimaryFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_primary, container, false);

        primaryPresenter = new PrimaryPresenter();
        primaryPresenter.attachView(this);
        primaryPresenter.getGrades();

        levelsParent = v.findViewById(R.id.levelsParent);

        return v;
    }

    @Override
    public void loadSubjects(final Grade grade, final List<Subject> subjects, int position) {
        calledTimes++;

        View subjectLevel = getActivity().getLayoutInflater().inflate(R.layout.subject_level_layout,null);
        TextView gradeName = subjectLevel.findViewById(R.id.gradeName);
        gradeName.setText(grade.getName());

        TextView studentCount = subjectLevel.findViewById(R.id.studentCount);
        studentCount.setText(grade.getNumberOfStudent()+"");

        TextView lessonCount = subjectLevel.findViewById(R.id.lessonCount);
        lessonCount.setText(grade.getNumberOfLessons()+"");

        LinearLayout subjectsParent = subjectLevel.findViewById(R.id.subjectsParent);

        for (int i=0; i<subjects.size(); i++){
            View subjectItem = getActivity().getLayoutInflater().inflate(R.layout.subject_item_layout,null);
            TextView subjectName = subjectItem.findViewById(R.id.subjectName);
            subjectName.setText(subjects.get(i).getName());
            ImageView icon = subjectItem.findViewById(R.id.subjectIcon);
            Picasso.get().load(subjects.get(i).getIconUrl()).into(icon);
            final int finalI = i;
            subjectItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseApplication.levelID = BaseApplication.levels.get(0).getId();
                    BaseApplication.gradeID = grade.getId();
                    BaseApplication.subject = subjects.get(finalI);
                    BaseApplication.subjectID = subjects.get(finalI).getId();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content,new SubjectFragment())
                            .addToBackStack(null)
                            .commit();
                }
            });
            subjectsParent.addView(subjectItem);
        }

        gradesViews[position] = subjectLevel;
        if (calledTimes == GradesCount)
            addViews();
    }

    @Override
    public void setGradesCount(int count) {
        calledTimes = 0;
        GradesCount = count;
        gradesViews = new View[count];
    }

    public void addViews(){
        for (int i=0; i<GradesCount; i++){
            levelsParent.addView(gradesViews[i]);
        }
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        primaryPresenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
