package xware.manaralsabeel.BrowseSubjects.Levels.SecondaryLevel;


import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Levels.Grade;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class SecondaryPresenter extends BasePresenter<SecondaryView> {

    public void getGrades(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getLevelGrades(BaseApplication.levels.get(2).getId())
                .enqueue(new Callback<List<Grade>>() {
                    @Override
                    public void onResponse(Call<List<Grade>> call, Response<List<Grade>> response) {
                        List<Grade> activeGrades = getActiveGrades(response.body());
                        if (activeGrades.size() == 0)
                            getView().hideProgress();
                        getView().setGradesCount(activeGrades.size());
                        getGradeSubjects(activeGrades);
                    }

                    @Override
                    public void onFailure(Call<List<Grade>> call, Throwable t) {
                        getView().hideProgress();
                        getView().showMessage(BaseApplication.error_msg);
                        Log.d("responseError",t.toString());
                    }
                });
    }

    private List<Grade> getActiveGrades(List<Grade> grades){
        List<Grade> activeGrades = new ArrayList<>();
        for (Grade grade : grades){
            if (grade.getIsActive())
                activeGrades.add(grade);
        }
        return activeGrades;
    }

    private void getGradeSubjects(final List<Grade> grades){
        for (int i=0; i<grades.size(); i++)
        {
            final int finalI = i;
            APIManager.getInstance().getAPI().getGradeSubjects(
                    grades.get(i).getId()).enqueue(new Callback<List<Subject>>() {
                @Override
                public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
                    getView().loadSubjects(grades.get(finalI),getActiveSubjects(response.body()),finalI);
                }

                @Override
                public void onFailure(Call<List<Subject>> call, Throwable t) {
                    getView().hideProgress();
                    getView().showMessage(BaseApplication.error_msg);
                    Log.d("responseError",t.toString());
                }
            });
        }
    }

    private List<Subject> getActiveSubjects(List<Subject> subjects){
        List<Subject> activeSubjects = new ArrayList<>();
        for (Subject subject : subjects){
            if (subject.getActive())
                activeSubjects.add(subject);
        }
        return activeSubjects;
    }
}
