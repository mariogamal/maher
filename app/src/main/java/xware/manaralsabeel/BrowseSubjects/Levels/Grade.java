package xware.manaralsabeel.BrowseSubjects.Levels;

import com.google.gson.annotations.SerializedName;

public class Grade {

    @SerializedName("id")
    private Integer id;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("name")
    private String name;

    @SerializedName("level")
    private Integer level;

    @SerializedName("studentsCount")
    private Integer numberOfStudent;

    @SerializedName("lessonsCount")
    private Integer numberOfLessons;

    public Grade() {
    }

    public Integer getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(Integer numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    public Integer getNumberOfLessons() {
        return numberOfLessons;
    }

    public void setNumberOfLessons(Integer numberOfLessons) {
        this.numberOfLessons = numberOfLessons;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

}
