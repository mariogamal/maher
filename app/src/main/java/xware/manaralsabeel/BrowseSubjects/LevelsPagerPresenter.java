package xware.manaralsabeel.BrowseSubjects;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Levels.Level;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class LevelsPagerPresenter extends BasePresenter<LevelsPagerView> {

    public void getLevels(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getLevels().enqueue(new Callback<List<Level>>() {
            @Override
            public void onResponse(Call<List<Level>> call, Response<List<Level>> response) {
                List<Level> activeLevels = getActiveLevels(response.body());
                BaseApplication.levels = activeLevels;
                getView().setTitles(activeLevels);
            }

            @Override
            public void onFailure(Call<List<Level>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }

    private List<Level> getActiveLevels (List<Level> levels){
        List<Level> activeLevels = new ArrayList<>();
        for (Level level : levels){
            if (level.getIsActive())
                activeLevels.add(level);
        }
        return activeLevels;
    }
}
