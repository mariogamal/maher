package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("name")
    private String name;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("id")
    private Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
