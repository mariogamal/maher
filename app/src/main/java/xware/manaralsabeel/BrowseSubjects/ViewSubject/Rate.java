package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import com.google.gson.annotations.SerializedName;

public class Rate {

    @SerializedName("id")
    private Integer id;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("rate")
    private Float rate;

    @SerializedName("teacher")
    private Integer teacher;

    @SerializedName("student")
    private Integer student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Integer getTeacher() {
        return teacher;
    }

    public void setTeacher(Integer teacher) {
        this.teacher = teacher;
    }

    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }
}
