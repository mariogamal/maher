package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface SubjectView extends BaseView {

    void displayTeachers(List<Teacher> teachers);

    void loadAddRemoveBtn(boolean exist);

    void showRateDialog(Rate rate, final String uuid);
}
