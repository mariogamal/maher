package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import com.google.gson.annotations.SerializedName;

public class Teacher {

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("school")
    private School school;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("id")
    private Integer id;

    @SerializedName("pictureUrl")
    private String pictureUrl;

    @SerializedName("averageRate")
    private Float rate;

    @SerializedName("idmUuid")
    private String uuid;

    @SerializedName("isSupervisor")
    private boolean isSupervisor;

    public Teacher() {
    }

    public String getFullName(){
        return firstName+" "+lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public void setSupervisor(boolean supervisor) {
        isSupervisor = supervisor;
    }
}
