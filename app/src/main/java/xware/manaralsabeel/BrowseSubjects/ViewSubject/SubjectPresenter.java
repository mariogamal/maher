package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import android.util.Log;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class SubjectPresenter extends BasePresenter<SubjectView> {

    public void loadTeachers(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getSubjectTeachers(BaseApplication.subjectID)
                .enqueue(new Callback<List<Teacher>>() {
            @Override
            public void onResponse(Call<List<Teacher>> call, Response<List<Teacher>> response) {
                BaseApplication.teachers = response.body();
                getView().displayTeachers(response.body());
            }

            @Override
            public void onFailure(Call<List<Teacher>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());

            }
        });
    }

    public void subjectRegist(){
        getView().showProgress();
        HashMap<String,Integer> hashMap =new HashMap<>();
        hashMap.put("student",BaseApplication.preferences.getInt("userID",0));
        APIManager.getInstance().getAPI().registerSubject(
                BaseApplication.subjectID,
                hashMap).enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap str =response.body();
                getView().hideProgress();
                getView().loadAddRemoveBtn(true);
                getStudentData();
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    public void subjectDelete(){
        getView().showProgress();
        APIManager.getInstance().getAPI().deleteSubject(
                BaseApplication.subjectID,
                BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<HashMap>() {
            @Override
            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
                HashMap str =response.body();
                getView().hideProgress();
                getView().loadAddRemoveBtn(false);
                getStudentData();
            }

            @Override
            public void onFailure(Call<HashMap> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    public void getSubjectExistence(){
        APIManager.getInstance().getAPI().checkSubjectExistence(
                BaseApplication.subjectID,
                BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<Subject>() {
            @Override
            public void onResponse(Call<Subject> call, Response<Subject> response) {
                if (response.body()!=null)
                    getView().loadAddRemoveBtn(true);
                else
                    getView().loadAddRemoveBtn(false);

            }

            @Override
            public void onFailure(Call<Subject> call, Throwable t) {
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }

    public void rateTeacher(String uuid, Float rateNum){
        HashMap<String,Float> rate = new HashMap<>();
        rate.put("rate",rateNum);
        APIManager.getInstance().getAPI().sendTeacherRate(uuid,rate).enqueue(new Callback<Rate>() {
            @Override
            public void onResponse(Call<Rate> call, Response<Rate> response) {
                loadTeachers();
            }

            @Override
            public void onFailure(Call<Rate> call, Throwable t) {

            }
        });
    }

    public void getTeacherRating(final String uuid){
        APIManager.getInstance().getAPI().getTeacherRate(uuid).enqueue(new Callback<Rate>() {
            @Override
            public void onResponse(Call<Rate> call, Response<Rate> response) {
                getView().showRateDialog(response.body(),uuid);
            }

            @Override
            public void onFailure(Call<Rate> call, Throwable t) {

            }
        });
    }
}
