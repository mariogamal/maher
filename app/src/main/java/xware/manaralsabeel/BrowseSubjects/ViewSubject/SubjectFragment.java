package xware.manaralsabeel.BrowseSubjects.ViewSubject;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.List;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.Live.LiveLessons.LiveLessonsFragment;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons.LessonFragment;
import xware.manaralsabeel.BrowseSubjects.ExamQuestions.ExamQuestionsFragment;
import xware.manaralsabeel.Conversations.ChatList.ChatsListFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.Profile.UserProfile;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.Dialogs.RegisterBlockDialog;

public class SubjectFragment extends BaseFragment implements SubjectView {

    LinearLayout teachersLayout, recordedLessons, liveLessons, subjectExams, teachersChats;
    HorizontalScrollView scrollView;
    SubjectPresenter subjectPresenter;
    Button addNremove;
    TextView subjectName;
    ImageView subjectIcon;
    public SubjectFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subject, container, false);
        teachersLayout = view.findViewById(R.id.teachersLayout);
        scrollView = view.findViewById(R.id.scroll);
        addNremove = view.findViewById(R.id.add_n_remove);
        recordedLessons = view.findViewById(R.id.recorded);
        liveLessons = view.findViewById(R.id.live);
        subjectExams = view.findViewById(R.id.exams);
        teachersChats = view.findViewById(R.id.teachers_chats);

        subjectName = view.findViewById(R.id.subject_name);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = view.findViewById(R.id.subject_icon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        subjectPresenter = new SubjectPresenter();
        subjectPresenter.attachView(this);
        subjectPresenter.loadTeachers();
        subjectPresenter.getSubjectExistence();

        recordedLessons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BaseApplication.isMySubject)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content, new LessonFragment())
                            .addToBackStack(null)
                            .commit();
                else
                    RegisterBlockDialog.getInstance().show(getActivity(), SubjectFragment.this);
            }
        });
        liveLessons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BaseApplication.isMySubject)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content, new LiveLessonsFragment())
                            .addToBackStack(null)
                            .commit();
                else
                    RegisterBlockDialog.getInstance().show(getActivity(), SubjectFragment.this);
            }
        });

        subjectExams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseApplication.isLessonQuestions = false;
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content, new ExamQuestionsFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        teachersChats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content, new ChatsListFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

    @Override
    public void displayTeachers(List<Teacher> teachers) {

        if (teachers != null) {
            teachersLayout.removeAllViews();
            for (final Teacher teacher : teachers) {
                View teacherItem = getActivity().getLayoutInflater().inflate(R.layout.teacher_item_layout, null);
                TextView name = teacherItem.findViewById(R.id.name);
                TextView school = teacherItem.findViewById(R.id.school);
                ImageView picture = teacherItem.findViewById(R.id.picture);
                RatingBar ratingBar = teacherItem.findViewById(R.id.teacherRate);
                ratingBar.setRating(teacher.getRate());
                String fullName = teacher.getFirstName() + " " + teacher.getLastName();
                name.setText(fullName);
                school.setText(teacher.getSchool().getName());
                Picasso.get().load(teacher.getPictureUrl()).into(picture);
                teacherItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showProgress();
                        subjectPresenter.getTeacherRating(teacher.getUuid());
                    }
                });
                teachersLayout.addView(teacherItem);

            }
            scrollView.postDelayed(new Runnable() {
                public void run() {
                    scrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                    hideProgress();
                }
            }, 10);
        }
        else
            hideProgress();
    }

    @Override
    public void loadAddRemoveBtn(boolean exist) {
        addNremove.setVisibility(View.VISIBLE);
        BaseApplication.isMySubject = exist;
        if (exist)
        {
            addNremove.setText(getActivity().getResources().getString(R.string.remove_from_my_courses));
            addNremove.setBackgroundColor(getActivity().getResources().getColor(R.color.russet));
            addNremove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    subjectPresenter.subjectDelete();
                }
            });
        }
        else
        {
            addNremove.setText(getActivity().getResources().getString(R.string.add_to_my_courses));
            addNremove.setBackgroundColor(getActivity().getResources().getColor(R.color.sap_green));
            addNremove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BaseApplication.preferences.getBoolean("isComplete", false))
                        subjectPresenter.subjectRegist();
                    else
                        showMessage(getResources().getString(R.string.complete_profile_first));
                }
            });
        }
    }

    @Override
    public void showRateDialog(Rate rate, final String uuid) {

        View rating = getActivity().getLayoutInflater().inflate(R.layout.teacher_rating_dialog, null);
        Button ratingBtn = rating.findViewById(R.id.rateBtn);
        RatingBar ratingBar = rating.findViewById(R.id.ratingBar);
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();

        if (rate != null)
            ratingBar.setRating(rate.getRate());

        ratingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectPresenter.rateTeacher(uuid, ratingBar.getRating());
                dialog.hide();
                showProgress();
            }
        });

        dialog.setView(rating);
        dialog.show();
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        subjectPresenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }

    public void registerInSubject(){
        subjectPresenter.subjectRegist();
    }

    public void openProfilePage(){
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new UserProfile())
                .addToBackStack(null)
                .commit();
    }
}
