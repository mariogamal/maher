package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import com.google.gson.annotations.SerializedName;

public class TeacherSubject {
    @SerializedName("teacher")
    private TeacherName teacher;

    public TeacherName getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherName teacher) {
        this.teacher = teacher;
    }
}
