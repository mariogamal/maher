package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import com.google.gson.annotations.SerializedName;

public class TeacherName {

    @SerializedName("id")
    private Integer id;

    @SerializedName("idmUuid")
    private String uuid;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return firstName+" "+lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
