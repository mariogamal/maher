package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.ExamQuestions.ExamQuestionsFragment;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments.Comment;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments.CommentsAdapter;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class RecordedDetailFragment extends BaseFragment implements RecordedDetailView {

    View postComment;
    EditText commentMessage;
    ListView commentsList;
    TextView subjectName;
    ImageView subjectIcon, sendBtn, downloadBtn;
    RecordedDetailPresenter presenter;
    LinearLayout lessonQuestions;

    public RecordedDetailFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recorded_detail, container, false);

        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.recorded_lessons));
        subjectName = view.findViewById(R.id.subjectTitle);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = view.findViewById(R.id.subjecticon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        postComment = view.findViewById(R.id.postComment);
        postComment.setVisibility(View.GONE);
        commentMessage = view.findViewById(R.id.commentMessage);

        downloadBtn = view.findViewById(R.id.download);
        if (BaseApplication.lesson.isDownloadable())
            downloadBtn.setVisibility(View.VISIBLE);
        else
            downloadBtn.setVisibility(View.GONE);
        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent downloadIntent = new Intent(Intent.ACTION_VIEW);
                downloadIntent.setData(Uri.parse(BaseApplication.lesson.getUrl()));
                startActivity(downloadIntent);
            }
        });

        sendBtn = view.findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!commentMessage.getText().toString().equals("")) {
                    presenter.postComment(commentMessage.getText().toString());
                    commentMessage.setText("");
                }
                else
                    showMessage(getString(R.string.write_comment_first));
            }
        });

        lessonQuestions = view.findViewById(R.id.lessonQuestions);
        lessonQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseApplication.isLessonQuestions = true;
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content,new ExamQuestionsFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        TextView lessonTitle = view.findViewById(R.id.lessonTitle);
        lessonTitle.setText(BaseApplication.titleTextNumber
                .concat(" : ").concat(BaseApplication.lesson.getTitle()));

        initLessonView(view);

        commentsList = view.findViewById(R.id.commentsList);
        initListFooter();

        presenter = new RecordedDetailPresenter();
        presenter.attachView(this);
        presenter.displayComments();

        return view;
    }

    @Override
    public void displayComments(List<Comment> comments) {
        if (comments != null){
            commentsList.setVisibility(View.VISIBLE);
            postComment.setVisibility(View.GONE);
            CommentsAdapter adapter = new CommentsAdapter(comments,getActivity());
            commentsList.setAdapter(adapter);
            hideProgress();
        }
        else
        {
            commentsList.setVisibility(View.GONE);
            postComment.setVisibility(View.VISIBLE);
        }
    }

    private void initLessonView(View view){
        TextView playText = view.findViewById(R.id.playText);
        ImageView playBtn = view.findViewById(R.id.playBtn);
        ImageView thumbNail = view.findViewById(R.id.thumbNail);
        if(BaseApplication.lesson.getPhotoUrl() != null)
            Picasso.get().load(BaseApplication.lesson.getPhotoUrl()).into(thumbNail);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content,new LessonViewFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        if (BaseApplication.lesson.getType().equals("video")) {
            playText.setText(R.string.click_here_to_watch_lesson);
            playBtn.setImageResource(R.drawable.playbtn);
        }
        else {
            playText.setText(R.string.click_here_to_read_lesson);
            playBtn.setImageResource(R.drawable.readbtn);
        }
    }

    private void initListFooter() {
        View footer = LayoutInflater.from(getActivity()).inflate(R.layout.post_comment_layout,null);
        final EditText comment = footer.findViewById(R.id.commentMessage);
        ImageView send = footer.findViewById(R.id.sendBtn);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!comment.getText().toString().equals("")) {
                    presenter.postComment(comment.getText().toString());
                    comment.setText("");
                }
                else
                    showMessage(getString(R.string.write_comment_first));
            }
        });
        commentsList.addFooterView(footer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
