package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class LessonsPresenter extends BasePresenter<LessonsView> {

    public void loadLessons(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getRecordedLessons(
                BaseApplication.subjectID,
                0).enqueue(new Callback<List<RecordedLesson>>() {
            @Override
            public void onResponse(Call<List<RecordedLesson>> call, Response<List<RecordedLesson>> response) {
                getView().displayLessons(getActiveLessons(response.body()));
            }

            @Override
            public void onFailure(Call<List<RecordedLesson>> call, Throwable t) {
                getView().hideProgress();

                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }

    private List<RecordedLesson> getActiveLessons (List<RecordedLesson> lessons){
        List<RecordedLesson> activeLessons = new ArrayList<>();
        for (RecordedLesson lesson : lessons){
            if (lesson.getActive())
                activeLessons.add(lesson);
        }
        return activeLessons;
    }
}
