package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments;

import com.google.gson.annotations.SerializedName;


public class TeacherNoSchool {

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("school")
    private Integer school;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("id")
    private Integer id;

    @SerializedName("pictureUrl")
    private String pictureUrl;


    public TeacherNoSchool() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getSchool() {
        return school;
    }

    public void setSchool(Integer school) {
        this.school = school;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
