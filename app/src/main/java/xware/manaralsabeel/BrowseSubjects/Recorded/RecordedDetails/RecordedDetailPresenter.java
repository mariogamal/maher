package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails;

import android.util.Log;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments.Comment;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;
import xware.manaralsabeel.R;

public class RecordedDetailPresenter extends BasePresenter<RecordedDetailView> {

    public void postComment(String commentText) {
        getView().showProgress();
        HashMap<String, String> comment = new HashMap<>();
        comment.put("text", commentText);
        comment.put("student", BaseApplication.preferences.getInt("userID", 0) + "");
        APIManager.getInstance().getAPI().postStudentComment(
                BaseApplication.lessonID,
                comment).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.getAppContext()
                    .getResources().getString(R.string.waiting_aproval));
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
            }
        });
    }

    public void displayComments() {
        getView().showProgress();
        APIManager.getInstance().getAPI().getLessonComments(
                BaseApplication.lessonID)
                .enqueue(new Callback<List<Comment>>() {
                    @Override
                    public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                        getView().displayComments(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Comment>> call, Throwable t) {
                        getView().hideProgress();
                        getView().showMessage(BaseApplication.error_msg);
                        Log.d("responseError", t.toString());
                    }
                });
    }
}
