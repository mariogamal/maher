package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class LessonFragment extends BaseFragment implements LessonsView {

    LessonsPresenter lessonsPresenter;
    ListView lessonsList;
    TextView subjectName;
    ImageView subjectIcon;
    public LessonFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lesson, container, false);
        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.recorded_lessons));
        subjectName = v.findViewById(R.id.subjectname);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = v.findViewById(R.id.subjecticon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        lessonsList = v.findViewById(R.id.lessonsList);
        lessonsPresenter = new LessonsPresenter();
        lessonsPresenter.attachView(this);
        lessonsPresenter.loadLessons();
        return v;
    }

    @Override
    public void displayLessons(List<RecordedLesson> lessons) {
        LessonsAdapter adapter = new LessonsAdapter(getActivity(),lessons);
        lessonsList.setAdapter(adapter);
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        lessonsPresenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
