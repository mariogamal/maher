package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;
import com.github.barteksc.pdfviewer.PDFView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import java.io.File;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.Dialogs.ProgressDialog;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class LessonViewFragment extends Fragment {

    ProgressBar progressBar;
    PDFView pdfView;
    WebView webView;
    VideoView videoView;
    final String MS_VIEWER = "https://view.officeapps.live.com/op/embed.aspx?src=";
    public LessonViewFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lesson_view, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        pdfView = view.findViewById(R.id.pdfView);
        webView = view.findViewById(R.id.webView);
        videoView = view.findViewById(R.id.videoView);
        if (BaseApplication.lesson.getType().equals("video"))
            initVideo();
        else {
            videoView.setVisibility(View.GONE);
            if (BaseApplication.lesson.getUrl().toLowerCase().endsWith(".pdf"))
                initPDF();
            else
                initOffice();
        }
        return view;
    }

    private void initVideo(){
        webView.setVisibility(View.GONE);
        pdfView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ((NavMenu)getActivity()).appBarLayout.setVisibility(View.GONE);
        videoView.setVideoPath(BaseApplication.lesson.getUrl());
        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();
        showProgress();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                hideProgress();
            }
        });
    }

    private void initPDF(){
        webView.setVisibility(View.GONE);
        if (checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        else
            downloadAndView();
    }

    private void downloadAndView(){
        Ion.with(getActivity())
                .load(BaseApplication.lesson.getUrl())
                .progressBar(progressBar)
                .write(new File(Environment.getExternalStorageDirectory().getPath().concat("/temp.pdf")))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {
                        progressBar.setVisibility(View.GONE);
                        if (result != null)
                            pdfView.fromFile(result).load();
                        else
                            Toast.makeText(getActivity(), BaseApplication.error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initOffice(){
        pdfView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.loadUrl(MS_VIEWER+BaseApplication.lesson.getUrl());
    }


    public void showProgress() {
        ProgressDialog.getInstance().show(getActivity());
    }

    public void hideProgress() {
        ProgressDialog.getInstance().dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ((NavMenu)getActivity()).appBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                downloadAndView();
            else
                Toast.makeText(getActivity(), "اذن الوصول للذاكرة مطلوب", Toast.LENGTH_SHORT).show();
        }
    }

}
