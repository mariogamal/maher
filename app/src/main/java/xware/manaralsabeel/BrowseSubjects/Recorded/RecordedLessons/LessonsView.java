package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface LessonsView extends BaseView {
    void displayLessons(List<RecordedLesson> lessons);
}
