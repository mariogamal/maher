package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import xware.manaralsabeel.R;

public class CommentsAdapter extends BaseAdapter {

    List<Comment> comments;
    Context context;

    public CommentsAdapter(List<Comment> comments, Context context) {
        this.comments = comments;
        this.context = context;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_item_layout,null);
            holder.icon = convertView.findViewById(R.id.icon);
            holder.name = convertView.findViewById(R.id.name);
            holder.comment = convertView.findViewById(R.id.commentText);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        if (comments.get(position).getTeacher() == null)
        {
            if (comments.get(position).getStudent().getPictureUrl() != null)
                Picasso.get().load(comments.get(position).getStudent().getPictureUrl()).into(holder.icon);
            else
                Picasso.get().load(R.drawable.circleicon).into(holder.icon);
            String name = comments.get(position).getStudent().getFullName();
            holder.name.setText(name);
            holder.comment.setText(comments.get(position).getText());
        }
        else{

            if (comments.get(position).getTeacher().getPictureUrl() != null)
                Picasso.get().load(comments.get(position).getTeacher().getPictureUrl()).into(holder.icon);
            else
                Picasso.get().load(R.drawable.circleicon).into(holder.icon);
            String name = comments.get(position).getTeacher().getFirstName()+" "
                    +comments.get(position).getTeacher().getLastName();
            holder.name.setText(name);
            holder.comment.setText(comments.get(position).getText());
        }
        return convertView;
    }

    class ViewHolder {
        CircleImageView icon;
        TextView name;
        TextView comment;
    }
}
