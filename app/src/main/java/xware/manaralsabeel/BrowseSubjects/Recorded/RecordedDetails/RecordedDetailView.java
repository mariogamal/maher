package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments.Comment;

public interface RecordedDetailView extends BaseView{

    void displayComments(List<Comment> comments);
}
