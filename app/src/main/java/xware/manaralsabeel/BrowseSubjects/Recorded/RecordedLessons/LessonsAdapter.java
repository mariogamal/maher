package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseHolder;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.RecordedDetailFragment;
import xware.manaralsabeel.Conversations.ChatList.ChatListAdapter;
import xware.manaralsabeel.R;

public class LessonsAdapter extends BaseAdapter {

    private Context context;
    private List<RecordedLesson> lessons;
    private HashMap<Integer, ViewHolder> holders;

    public LessonsAdapter(Context context, List<RecordedLesson> lessons) {
        this.context = context;
        this.lessons = lessons;
        holders = new HashMap<>();
    }

    @Override
    public int getCount() {
        return lessons.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int index, View convertView, ViewGroup parent) {
        ViewHolder holder = holders.get(index);
        if (holder == null) {
            holder = new ViewHolder(convertView);
        }
        holder = (ViewHolder) BaseHolder.constructHolder(context, convertView, holder, R.layout.reccorded_item_layout, index);

        holders.put(index, holder);
        return holder.convertView;
    }

    class ViewHolder extends BaseHolder{
        LinearLayout lessonCell;
        TextView lessonNumber;
        TextView lessonTitle;
        TextView teacherName;
        TextView lessonLevel;
        ImageView lessonIcon;
        ImageView levelIcon;

        public ViewHolder(View convertView) {
            super(convertView);
        }

        @Override
        public void initViews() {
            lessonCell = convertView.findViewById(R.id.lesson_cell);
            lessonNumber = convertView.findViewById(R.id.lesson_number);
            lessonTitle = convertView.findViewById(R.id.title);
            teacherName = convertView.findViewById(R.id.teacher);
            lessonLevel = convertView.findViewById(R.id.level);
            lessonIcon = convertView.findViewById(R.id.icon);
            levelIcon = convertView.findViewById(R.id.diffculty);
        }

        @Override
        public void setValues(int index) {
            String[] lessonsNumber = context.getResources().getStringArray(R.array.lessons);
            lessonNumber.setText(lessonsNumber[index]);
            lessonTitle.setText(lessons.get(index).getTitle());
            teacherName.setText(lessons.get(index).getTeacherSubject().getTeacher().getName());
            lessonLevel.setText(lessons.get(index).getLevel());
            levelIcon.setImageResource(getDiffcultyImage(lessons.get(index).getLevel()));
            if (lessons.get(index).getType().equals("video"))
                lessonIcon.setImageResource(R.drawable.play);
            else
                lessonIcon.setImageResource(R.drawable.bookmark);
            final String title = lessons.get(index).getTitle();
            final String[] finalLessonsNumber = lessonsNumber;
            lessonCell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseApplication.titleTextNumber = finalLessonsNumber[index];
                    BaseApplication.lesson  = lessons.get(index);
                    BaseApplication.lessonID = lessons.get(index).getId();
                    BaseApplication.TeacherID = lessons.get(index).getTeacherSubject().getTeacher().getUuid();
                    ((AppCompatActivity)context).getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content,new RecordedDetailFragment())
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }


    private int getDiffcultyImage(String level) {
        switch (level)
        {
            case "مبتدئ":
                return R.drawable.difficulty;

            case "متوسط":
                return R.drawable.difficultytwo;

            case "متفوق":
                return R.drawable.difficultythree;

            case "متميز":
                return R.drawable.difficultyfour;

            case "عبقرى":
                return R.drawable.difficultyfive;

            default:
                return R.drawable.difficulty;
        }
    }
}
