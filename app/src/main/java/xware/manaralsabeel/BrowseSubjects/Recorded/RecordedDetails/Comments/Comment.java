package xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments;

import com.google.gson.annotations.SerializedName;

import xware.manaralsabeel.Profile.Student;

public class Comment {

    @SerializedName("id")
    private Integer id;

    @SerializedName("text")
    private String text;

    @SerializedName("student")
    private Student student;

    @SerializedName("teacher")
    private TeacherNoSchool teacher;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public TeacherNoSchool getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherNoSchool teacher) {
        this.teacher = teacher;
    }
}
