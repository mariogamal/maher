package xware.manaralsabeel.Notifications.NotifyServices;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

import static xware.manaralsabeel.Splash.TAG;

public class RecieveMessage extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> data = new HashMap<>();
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            data = remoteMessage.getData();
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            pushNotification(remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(), data);
        }
        BaseApplication.getAppContext().sendBroadcast(new Intent("NEW_NOTIFICATION"));
    }

    private void pushNotification(String title, String body, Map<String, String> data) {
        String CHANNEL_ID = "Channel01";
        int requestID = (int) System.currentTimeMillis();
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.circleicon);
        Intent notificationIntent = new Intent(getApplicationContext(), NavMenu.class);
        notificationIntent.putExtra("object", "");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.notifyicon)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setLargeIcon(largeIcon)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "Elsabeel Channel";
            String Description = "Notification channel01";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(requestID, mBuilder.build());
    }
}
