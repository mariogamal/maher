package xware.manaralsabeel.Notifications.NotificationPage;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.List;

import xware.manaralsabeel.R;

public class NotificationSwipeAdapter extends BaseSwipeAdapter {

    private List<Notification> notifications;
    private Fragment notificationFragment;
    private Context context;
    private ViewHolder holder;

    NotificationSwipeAdapter(List<Notification> notifications, Fragment notificationFragment, Context context) {
        this.notifications = notifications;
        this.notificationFragment = notificationFragment;
        this.context = context;
    }

    class ViewHolder {
        TextView title;
        ImageView icon;
        ImageView seenBtn;
        LinearLayout notificationLayout;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void appendNewList(List<Notification> newNotifications) {
        notifications.addAll(newNotifications);
        notifyDataSetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        View cellView = LayoutInflater.from(context).inflate(R.layout.notification_item_layout, null);
        holder = new ViewHolder();
        holder.title = cellView.findViewById(R.id.title);
        holder.icon = cellView.findViewById(R.id.notification_icon);
        holder.seenBtn = cellView.findViewById(R.id.mark_seen);
        holder.notificationLayout = cellView.findViewById(R.id.notification_layout);
        cellView.setTag(holder);
        return cellView;
    }

    @Override
    public void fillValues(int index, View cellView) {
        holder = (ViewHolder) cellView.getTag();
        decorateNotificationLayout(notifications.get(index).getSeen());
        String titleText = notifications.get(index).getNotification().get("title");
        holder.title.setText(titleText);

        if (titleText.contains(context.getString(R.string.congratulations)))
            holder.icon.setImageResource(R.drawable.medal);
        else
            holder.icon.setImageResource(R.drawable.checkmark);

        //SwipeLayout not a holder variable duo to library bug
        SwipeLayout swipeLayout = cellView.findViewById(R.id.swipeLayout);
        holder.seenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeLayout.close();
                if (!notifications.get(index).getSeen())
                    ((NotificationFragment) notificationFragment).markAsSeen(notifications.get(index).getId(), index);
            }
        });
    }

    private void decorateNotificationLayout(boolean isSeen) {
        Typeface cairoRegular = Typeface.createFromAsset(context.getAssets(),
                context.getString(R.string.cairo_regular_font));
        Typeface cairoBold = Typeface.createFromAsset(context.getAssets(),
                context.getString(R.string.cairo_bold_font));

        if (isSeen) {
            holder.title.setTypeface(cairoRegular);
            holder.seenBtn.setImageResource(R.drawable.seenmark);
            holder.notificationLayout.setBackgroundColor(context.getResources().getColor(R.color.lightGray));
        } else {
            holder.title.setTypeface(cairoBold);
            holder.seenBtn.setImageResource(R.drawable.mark);
            holder.notificationLayout.setBackgroundColor(context.getResources().getColor(R.color.gray));
        }
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Object getItem(int index) {
        return notifications.get(index);
    }

    @Override
    public long getItemId(int index) {
        return notifications.get(index).getId();
    }

    public void updateNotificationItem(int index, Notification notification) {
        notifications.set(index, notification);
        notifyDataSetChanged();
    }
}
