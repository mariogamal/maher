package xware.manaralsabeel.Notifications.NotificationPage;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class NotificationPresenter extends BasePresenter<NotificationView> {

    private boolean hasNext, requestInProcess;
    private int pageNumber = 1;

    public int getPageNumber() {
        return pageNumber;
    }

    public boolean hasNext() {
        return hasNext;
    }

    public void getNotifications(Integer queryPageNumber) {
        if (pageNumber == 1)
            getView().showProgress();
        requestInProcess = true;
        APIManager.getInstance().getAPI().getNotifications(queryPageNumber).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                requestInProcess = false;
                getView().displayNotifications(response.body().getResults());
                if (response.body().getNext() != null) {
                    pageNumber++;
                    hasNext = true;
                } else {
                    getView().removeLoadingFooter();
                    hasNext = false;
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }

    public void markNotificationAsSeen(Integer id, int index) {
        getView().showProgress();
        HashMap<String, Boolean> isSeen = new HashMap<>();
        isSeen.put("is_seen", true);
        APIManager.getInstance().getAPI().markNotificationAsSeen(id, isSeen).enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                getView().notifyNotificationsAdapter(index, response.body());
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }

    public void getNextPage() {
        if (!requestInProcess)
            getNotifications(pageNumber);
    }
}
