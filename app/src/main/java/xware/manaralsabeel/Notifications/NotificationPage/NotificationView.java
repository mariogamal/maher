package xware.manaralsabeel.Notifications.NotificationPage;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface NotificationView extends BaseView {

    void removeLoadingFooter();

    void displayNotifications(List<Notification> notifications);

    void notifyNotificationsAdapter(int index, Notification notification);
}
