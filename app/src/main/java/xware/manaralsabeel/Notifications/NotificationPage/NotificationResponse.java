package xware.manaralsabeel.Notifications.NotificationPage;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("count")
    private Integer count;

    @SerializedName("next")
    private String next;

    @SerializedName("previous")
    private String previous;

    @SerializedName("results")
    private List<Notification> results = null;

    @SerializedName("notSeenCount")
    private Integer notSeenCount;

    public Integer getNotSeenCount() {
        return notSeenCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<Notification> getResults() {
        return results;
    }

    public void setResults(List<Notification> results) {
        this.results = results;
    }
}
