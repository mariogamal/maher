package xware.manaralsabeel.Notifications.NotificationPage;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

import retrofit2.http.Body;

public class Notification {

    @SerializedName("id")
    Integer id;

    @SerializedName("data")
    HashMap<String, String> data;

    @SerializedName("details")
    HashMap<String, String> notification;

    @SerializedName("isSeen")
    Boolean isSeen;

    public Integer getId() {
        return id;
    }

    public Boolean getSeen() {
        return isSeen;
    }

    public void setSeen(Boolean seen) {
        isSeen = seen;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public HashMap<String, String> getNotification() {
        return notification;
    }

}
