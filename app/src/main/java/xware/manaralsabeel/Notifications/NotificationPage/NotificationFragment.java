package xware.manaralsabeel.Notifications.NotificationPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import xware.manaralsabeel.Achievments.AchievementsFragment;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.MySubjects.MySubjectsFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.ScrollListView;

public class NotificationFragment extends BaseFragment implements NotificationView {

    TextView noNotifications;
    View footerProgressDialog;
    ScrollListView notificationList;
    NotificationPresenter presenter;
    NotificationSwipeAdapter swipeAdapter;

    public NotificationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        footerProgressDialog = inflater.inflate(R.layout.progressbar_dialog_layout, null);

        ((NavMenu) getActivity()).setTitleText(getResources().getString(R.string.notifications));
        noNotifications = view.findViewById(R.id.noNotifications);
        notificationList = view.findViewById(R.id.notificationList);
        notificationList.addFooterView(footerProgressDialog);
        notificationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                if (swipeAdapter.getNotifications().get(index)
                        .getNotification().get("title").contains(getString(R.string.congratulations)))
                    beginFragmentTransaction(new AchievementsFragment());
                else
                    beginFragmentTransaction(new MySubjectsFragment());
            }
        });
        notificationList.setOnBottomReachedListener(new ScrollListView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if (presenter.hasNext())
                    presenter.getNextPage();
                else
                    removeLoadingFooter();
            }
        });
        presenter = new NotificationPresenter();
        presenter.attachView(this);
        presenter.getNotifications(null);
        return view;
    }

    @Override
    public void removeLoadingFooter() {
        notificationList.removeFooterView(footerProgressDialog);
    }

    @Override
    public void displayNotifications(List<Notification> notifications) {
        if (presenter.getPageNumber() == 1) {
            swipeAdapter = new NotificationSwipeAdapter(notifications, this, getActivity());
            notificationList.setAdapter(swipeAdapter);
            if (notifications.size() == 0)
                noNotifications.setVisibility(View.VISIBLE);
        } else
            swipeAdapter.appendNewList(notifications);

        hideProgress();
    }

    @Override
    public void notifyNotificationsAdapter(int index, Notification notification) {
        hideProgress();
        swipeAdapter.updateNotificationItem(index, notification);
    }

    public void markAsSeen(int id, int index) {
        presenter.markNotificationAsSeen(id, index);
    }

    private void beginFragmentTransaction(Fragment fragment) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content, fragment)
                .addToBackStack(null)
                .commit();
    }

}
