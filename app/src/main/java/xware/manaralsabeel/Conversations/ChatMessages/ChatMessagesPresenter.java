package xware.manaralsabeel.Conversations.ChatMessages;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class ChatMessagesPresenter extends BasePresenter<ChatMessagesView> {

    private boolean hasNext, hasPrevious, requestInProcess;
    private int pageNumber = 1;

    public int getPageNumber() {
        return pageNumber;
    }

    public boolean hasNext() {
        return hasNext;
    }

    public boolean hasPrevious() {
        return hasPrevious;
    }

    public void getChatMessages(Integer queryPageNumber) {
        if (pageNumber == 1)
            getView().showProgress();
        String studentUuid = BaseApplication.preferences.getString("idmUuid", null);
        String teacherUuid = BaseApplication.teacher.getUuid();
        requestInProcess = true;
        APIManager.getInstance().getAPI().getChatMessages(studentUuid, teacherUuid, queryPageNumber)
                .enqueue(new Callback<ChatResponse>() {
                    @Override
                    public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                        requestInProcess = false;
                        hasPrevious = response.body().getPrevious() != null;
                        getView().displayMessages(response.body().getResults());
                        if (response.body().getNext() != null) {
                            pageNumber++;
                            hasNext = true;
                        } else {
                            getView().removeLoadingView();
                            hasNext = false;
                        }
                    }

                    @Override
                    public void onFailure(Call<ChatResponse> call, Throwable t) {
                        getView().hideProgress();
                        getView().showMessage(t.toString());
                    }
                });
    }

    public void getNextPage() {
        if (!requestInProcess)
            getChatMessages(pageNumber);
    }
}
