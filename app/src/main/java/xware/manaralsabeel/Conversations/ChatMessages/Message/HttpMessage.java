package xware.manaralsabeel.Conversations.ChatMessages.Message;

import com.google.gson.annotations.SerializedName;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Conversations.ChatMessages.Message.Message;

public class HttpMessage extends Message {

    @SerializedName("id")
    private Integer id;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("message")
    private String message;

    @SerializedName("senderUuid")
    private String senderUUID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderUUID() {
        return senderUUID;
    }

    public void setSenderUUID(String senderUUID) {
        this.senderUUID = senderUUID;
    }

    public boolean isMyMessage() {
        return senderUUID.equals(BaseApplication.preferences.getString("idmUuid", null));
    }

}
