package xware.manaralsabeel.Conversations.ChatMessages;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import xware.manaralsabeel.Conversations.ChatMessages.Message.HttpMessage;

public class ChatResponse {

    @SerializedName("count")
    private Integer count;

    @SerializedName("next")
    private String next;

    @SerializedName("previous")
    private String previous;

    @SerializedName("results")
    private List<HttpMessage> results = null;

    public Integer getCount() {
        return count;
    }

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }

    public List<HttpMessage> getResults() {
        return results;
    }
}
