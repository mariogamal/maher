package xware.manaralsabeel.Conversations.ChatMessages.Message;

import com.google.gson.annotations.SerializedName;

import xware.manaralsabeel.Base.BaseApplication;

public class WSMessage extends Message {

    @SerializedName("id")
    private Integer id;

    @SerializedName("message")
    private String message;

    @SerializedName("sender_uuid")
    private String sender_uuid;

    @SerializedName("created_at")
    private String created_at;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getCreatedAt() {
        return created_at;
    }

    @Override
    public boolean isMyMessage() {
        return sender_uuid.equals((BaseApplication.preferences.getString("idmUuid", null)));
    }
}
