package xware.manaralsabeel.Conversations.ChatMessages;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import xware.manaralsabeel.Base.BaseHolder;
import xware.manaralsabeel.Conversations.ChatMessages.Message.HttpMessage;
import xware.manaralsabeel.Conversations.ChatMessages.Message.Message;
import xware.manaralsabeel.R;

public class ChatMessagesAdapter extends BaseAdapter {

    private List<Message> chatMessages;
    private Context context;
    private HashMap<Integer, ViewHolder> holders;

    ChatMessagesAdapter(List<HttpMessage> chatMessages, Context context) {
        this.chatMessages = new ArrayList<>();
        this.chatMessages.addAll(chatMessages);
        Collections.reverse(this.chatMessages);
        this.context = context;
        holders = new HashMap<>();
    }

    public List<Message> getChatMessages() {
        return chatMessages;
    }

    public void appendNewList(List<HttpMessage> messages){
        Collections.reverse(messages);
        chatMessages.addAll(0, messages);
        notifyDataSetChanged();
    }

    class ViewHolder {
        LinearLayout cellLayout;
        LinearLayout msgLayout;
        TextView text;
        TextView time;
        TextView date;
    }

    @Override
    public int getCount() {
        return chatMessages.size();
    }

    @Override
    public Object getItem(int index) {
        return chatMessages.get(index);
    }

    @Override
    public long getItemId(int index) {
        return -1;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.chat_message_item, null);
            holder.cellLayout = convertView.findViewById(R.id.cellLayout);
            holder.msgLayout = convertView.findViewById(R.id.msgLayout);
            holder.text = convertView.findViewById(R.id.text);
            holder.time = convertView.findViewById(R.id.time);
            holder.date = convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        decorateMessageLayout(holder, chatMessages.get(index).isMyMessage());
        holder.text.setText(chatMessages.get(index).getMessage());
        holder.time.setText(dateTimeFormatter(chatMessages.get(index).getCreatedAt(), false));
        String currentDay = dateTimeFormatter(chatMessages.get(index).getCreatedAt(), true);
        String previousDay = index != 0 ?
                dateTimeFormatter(chatMessages.get(index-1).getCreatedAt(), true) : "";
        if (currentDay.equals(previousDay))
            holder.date.setVisibility(View.GONE);
        else
            holder.date.setVisibility(View.VISIBLE);
        holder.date.setText(currentDay);
        holders.put(index, holder);
        return convertView;
    }

    private void decorateMessageLayout(ViewHolder holder, boolean isMyMessage){
        if (isMyMessage) {
            holder.text.setTextColor(context.getResources().getColor(R.color.white));
            holder.time.setTextColor(context.getResources().getColor(R.color.white));
            holder.cellLayout.setGravity(Gravity.RIGHT);
            holder.msgLayout.setLayoutParams(getMargin());
            holder.msgLayout.setBackground(context.getResources().getDrawable(R.drawable.blue_round_corners));

        }
        else {
            holder.text.setTextColor(context.getResources().getColor(R.color.navy));
            holder.time.setTextColor(context.getResources().getColor(R.color.darkGray));
            holder.cellLayout.setGravity(Gravity.LEFT);
            holder.msgLayout.setLayoutParams(getMargin());
            holder.msgLayout.setBackground(context.getResources().getDrawable(R.drawable.white_round_corners));
        }
    }

    private LinearLayout.LayoutParams getMargin(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        int marginValue = (int) context.getResources().getDimension(R.dimen._5sdp);
        layoutParams.setMargins(marginValue, marginValue, marginValue, marginValue);
        return layoutParams;
    }

    private String dateTimeFormatter(String date, boolean isDate){
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX");
        String output = isDate ? "EEEE, yyyy/MM/dd" : "h:mm a";
        DateFormat outputFormat = new SimpleDateFormat(output);
        Date formattedDate = null;
        try {
            formattedDate = inputFormat.parse(date);
        } catch (ParseException e) {
            inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSX");
            try {
                formattedDate = inputFormat.parse(date);
            }
            catch (Exception ignore){}
        }
        return outputFormat.format(formattedDate);
    }
}
