package xware.manaralsabeel.Conversations.ChatMessages;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.Conversations.ChatMessages.Message.HttpMessage;

public interface ChatMessagesView extends BaseView {

    void removeLoadingView();

    void displayMessages(List<HttpMessage> messages);
}
