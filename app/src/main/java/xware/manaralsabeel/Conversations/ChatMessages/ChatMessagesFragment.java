package xware.manaralsabeel.Conversations.ChatMessages;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.Conversations.ChatMessages.Message.HttpMessage;
import xware.manaralsabeel.Conversations.ChatMessages.Message.Message;
import xware.manaralsabeel.Conversations.ChatMessages.Message.WSMessage;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.NetworkAPI.Websocket.ServerConnection;
import xware.manaralsabeel.R;

public class ChatMessagesFragment extends BaseFragment
        implements ServerConnection.ServerListener, ChatMessagesView {

    private final String STUDENT_UUID = BaseApplication.preferences.getString("idmUuid", null);
    private final String TEACHER_UUID = BaseApplication.teacher.getUuid();
    private ServerConnection mServerConnection;
    private ChatMessagesPresenter presenter;
    private ListView messagesList;
    private Gson jsonSerializer;
    private ChatMessagesAdapter chatAdapter;
    private EditText chatMessage;
    private ProgressBar progressBar;
    private int currentScrollState, currentVisibleItemCount, currentFirstVisibleItem;

    public ChatMessagesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_messages, container, false);
        progressBar = view.findViewById(R.id.progress_bar);
        jsonSerializer = new GsonBuilder().create();
        ((NavMenu) getActivity()).setTitleText(getResources().getString(R.string.conversations));
        initChatView(view);
        mServerConnection = new ServerConnection(STUDENT_UUID, TEACHER_UUID);
        presenter = new ChatMessagesPresenter();
        presenter.attachView(this);
        presenter.getChatMessages(null);
        return view;
    }

    private void initChatView(View view) {
        messagesList = view.findViewById(R.id.messages);
        messagesList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                if (currentVisibleItemCount > 0 && currentScrollState == SCROLL_STATE_IDLE) {
                    if (currentFirstVisibleItem == 0) {
                        if (presenter.hasNext()) {
                            progressBar.setVisibility(View.VISIBLE);
                            presenter.getNextPage();
                        } else
                            removeLoadingView();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                currentVisibleItemCount = visibleItemCount;
            }
        });
        chatMessage = view.findViewById(R.id.chatMessage);
        ImageView sendBtn = view.findViewById(R.id.sendBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(chatMessage.getText()))
                    sendMessage(chatMessage.getText().toString());
                else
                    showMessage(getResources().getString(R.string.please_write_message));
            }
        });
    }

    private void sendMessage(String message) {
        JSONObject jsonMessage = new JSONObject();
        try {
            jsonMessage.put("message", message);
            jsonMessage.put("sender_uuid", STUDENT_UUID);
        } catch (JSONException ignore) {
            showMessage(BaseApplication.error_msg);
        }
        mServerConnection.sendMessage(jsonMessage.toString());
        chatMessage.setText("");
    }

    @Override
    public void onNewMessage(String message) {
        WSMessage parsedMessage = jsonSerializer.fromJson(message, WSMessage.class);
        chatAdapter.getChatMessages().add(parsedMessage);
        chatAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStatusChange(ServerConnection.ConnectionStatus status) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mServerConnection.connect(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mServerConnection.disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void removeLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayMessages(List<HttpMessage> messages) {

        if (messages != null) {
            if (!presenter.hasPrevious()) {
                chatAdapter = new ChatMessagesAdapter(messages, getActivity());
                messagesList.setAdapter(chatAdapter);
            } else {
                removeLoadingView();
                chatAdapter.appendNewList(messages);
            }
        }
        hideProgress();
    }
}
