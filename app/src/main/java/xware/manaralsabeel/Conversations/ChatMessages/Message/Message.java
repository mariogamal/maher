package xware.manaralsabeel.Conversations.ChatMessages.Message;

public abstract class Message {

    public abstract Integer getId();

    public abstract String getMessage();

    public abstract String getCreatedAt();

    public abstract boolean isMyMessage();
}
