package xware.manaralsabeel.Conversations.ChatList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import xware.manaralsabeel.Base.BaseHolder;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.Teacher;
import xware.manaralsabeel.Conversations.ChatMessages.ChatMessagesAdapter;
import xware.manaralsabeel.R;

public class ChatListAdapter extends BaseAdapter {

    List<Teacher> teachers;
    Context context;
    private HashMap<Integer, ViewHolder> holders;

    public ChatListAdapter(List<Teacher> teachers, Context context) {
        this.teachers = teachers;
        this.context = context;
        holders = new HashMap<>();
    }

    @Override
    public int getCount() {
        return teachers.size();
    }

    @Override
    public Object getItem(int index) {
        return teachers.get(index);
    }

    @Override
    public long getItemId(int index) {
        return teachers.get(index).getId();
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        ViewHolder holder = holders.get(index);
        if (holder == null) {
            holder = new ViewHolder(convertView);
        }
        holder = (ViewHolder) BaseHolder.constructHolder(context, convertView, holder, R.layout.chat_list_item, index);
        holder.name.setText(teachers.get(index).getFullName());
        String title = teachers.get(index).isSupervisor()
                ? context.getString(R.string.supervisor)
                : context.getString(R.string.teacher);
        holder.title.setText(title + " - " + teachers.get(index).getSchool().getName());
        Picasso.get().load(teachers.get(index).getPictureUrl())
                .error(R.drawable.profileicon)
                .placeholder(R.drawable.profileicon)
                .into(holder.picture);
        holders.put(index, holder);
        return holder.convertView;
    }

    class ViewHolder extends BaseHolder {
        TextView name;
        TextView title;
        CircleImageView picture;

        public ViewHolder(View convertView) {
            super(convertView);
        }

        @Override
        public void initViews() {
            name = this.convertView.findViewById(R.id.name);
            title = this.convertView.findViewById(R.id.title);
            picture = this.convertView.findViewById(R.id.picture);
        }

        @Override
        public void setValues(int index) {

        }
    }
}
