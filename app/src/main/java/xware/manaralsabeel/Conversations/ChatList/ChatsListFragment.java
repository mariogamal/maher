package xware.manaralsabeel.Conversations.ChatList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.Conversations.ChatMessages.ChatMessagesFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class ChatsListFragment extends BaseFragment {

    TextView subjectName;
    ImageView subjectIcon;
    public ChatsListFragment() {  }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats_home, container, false);
        ((NavMenu)getActivity()).setTitleText(getResources().getString(R.string.conversations));
        subjectName = view.findViewById(R.id.subjectname);
        subjectName.setText(BaseApplication.subject.getName());
        subjectIcon = view.findViewById(R.id.subjecticon);
        Picasso.get().load(BaseApplication.subject.getIconUrl()).into(subjectIcon);

        ListView teachersList = view.findViewById(R.id.teachersList);
        ChatListAdapter adapter = new ChatListAdapter(BaseApplication.teachers, getActivity());
        teachersList.setAdapter(adapter);

        teachersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BaseApplication.teacher = BaseApplication.teachers.get(position);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content, new ChatMessagesFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        return view;
    }
}
