package xware.manaralsabeel.Home;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import xware.manaralsabeel.PhoneAuth.PhoneValidation;
import xware.manaralsabeel.R;

public class HomeActivity extends AppCompatActivity {

    TextView start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        start = findViewById(R.id.startNow);
        start.setVisibility(View.INVISIBLE);

        ViewPager viewPager = findViewById(R.id.HomePager);
        viewPager.setAdapter(new HomePagerAdapter(this));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2)
                    start.setVisibility(View.VISIBLE);
                else
                    start.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = findViewById(R.id.HomeTapDots);
        tabLayout.setupWithViewPager(viewPager,true);
    }

    public void startNow(View view) {
        finish();
        startActivity(new Intent(this, PhoneValidation.class));
    }
}
