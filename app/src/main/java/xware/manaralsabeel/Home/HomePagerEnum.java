package xware.manaralsabeel.Home;

import xware.manaralsabeel.R;

public enum HomePagerEnum {
    First(R.layout.home_screen_first),
    Second(R.layout.home_screen_second),
    Third(R.layout.home_screen_third);

    private int mLayoutResId;

    HomePagerEnum(int layoutResId) {
        mLayoutResId = layoutResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }
}
