package xware.manaralsabeel.PhoneAuth;

import xware.manaralsabeel.Base.BaseView;

public interface CodeValidationView extends BaseView {

    void updateCounter(int seconds);

    void showCodeResend();

    void hideCodeResend();

    void openApplication();

}
