package xware.manaralsabeel.PhoneAuth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import xware.manaralsabeel.Base.BaseActivity;
import xware.manaralsabeel.DocumentsAgreement.DocumentsAgreementActivity;
import xware.manaralsabeel.R;

public class CodeValidation extends BaseActivity implements CodeValidationView {

    CodeValidationPresenter presenter;
    TextView timerCount, resendCode;
    String phone;
    EditText code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_validation);
        Intent intent = getIntent();
        phone = intent.getStringExtra("phone");

        timerCount = findViewById(R.id.timerCount);
        resendCode = findViewById(R.id.resendCode);
        resendCode.setVisibility(View.INVISIBLE);
        code = findViewById(R.id.code);


        presenter = new CodeValidationPresenter();
        presenter.attachView(this);
        presenter.sendCode(phone);

    }

    @Override
    public void updateCounter(int seconds) {
        timerCount.setText(String.valueOf(seconds));
    }

    @Override
    public void showCodeResend() {
        resendCode.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCodeResend() {
        resendCode.setVisibility(View.INVISIBLE);
    }

    @Override
    public void openApplication() {
        finish();
        startActivity(new Intent(this, DocumentsAgreementActivity.class));
    }

    public void callValidation(View view) {
        presenter.validateCode(code.getText().toString());
    }

    public void resendCode(View view) {
        presenter.sendCode(phone);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this,PhoneValidation.class));
    }
}
