package xware.manaralsabeel.PhoneAuth;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;
import xware.manaralsabeel.Profile.Student;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.JWTDecoder;

public class CodeValidationPresenter extends BasePresenter<CodeValidationView> {

    private String verificationId;
    private FirebaseAuth mAuth;

    CodeValidationPresenter() {
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("ar");
    }

    public void sendCode(String phone) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,                 // Phone number to verify
                30,                  // Timeout duration
                TimeUnit.SECONDS,      // Unit of timeout
                (Activity) getView(),  // Activity (for callback binding)
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                        Toast.makeText(BaseApplication.getAppContext(), "تم التحقق بنجاح", Toast.LENGTH_SHORT).show();
                        //Log.d("fireBaseResponse", "completed");
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException exception) {
                        //Log.d("fireBaseResponse", exception.toString());
                        if (exception instanceof FirebaseAuthInvalidCredentialsException)
//                            getView().showMessage(BaseApplication.getAppContext()
//                                .getString(R.string.enter_valid_number));
                            Toast.makeText(BaseApplication.getAppContext(), "رقم الهاتف غير صحيح", Toast.LENGTH_SHORT).show();

//                        else
//                            Toast.makeText(BaseApplication.getAppContext(), exception.toString(), Toast.LENGTH_SHORT).show();
                        //getView().showMessage(BaseApplication.error_msg);
                    }

                    @Override
                    public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        Toast.makeText(BaseApplication.getAppContext(), "تم ارسال الكود", Toast.LENGTH_SHORT).show();
                        //Log.d("fireBaseResponse", "code sent");
                        verificationId = s;
                    }
                });
        requestTimer();
    }

    public void validateCode(String code) {
        getView().showProgress();
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception exception) {
            getView().hideProgress();
            getView().showMessage(BaseApplication.error_msg);
        }
    }

    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) getView(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();

                            BaseApplication.editor.putString("phone", user.getPhoneNumber());
                            BaseApplication.editor.putString("password", user.getUid());
                            BaseApplication.editor.commit();

                            String token = FirebaseInstanceId.getInstance().getToken();

                            HashMap<String, String> credentials = new HashMap<>();
                            credentials.put("phone", user.getPhoneNumber());
                            credentials.put("password", user.getUid());
                            credentials.put("device_token", FirebaseInstanceId.getInstance().getToken());

                            APIManager.getInstance().getAPI().authStudent(credentials).enqueue(new Callback<Authorization>() {
                                @Override
                                public void onResponse(Call<Authorization> call, Response<Authorization> response) {

                                    Authorization auth = response.body();
                                    BaseApplication.editor.putString("Authorization", auth.getIdToken());
                                    BaseApplication.editor.putString("id", JWTDecoder.decoded(auth.getIdToken()));
                                    BaseApplication.editor.commit();
                                    getUserID(JWTDecoder.decoded(auth.getIdToken()));
                                }

                                @Override
                                public void onFailure(Call<Authorization> call, Throwable t) {
                                    getView().hideProgress();
                                    getView().showMessage(BaseApplication.error_msg);
                                }
                            });
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                getView().showMessage(((Activity) getView()).getString(R.string.wrong_code));
                                getView().hideProgress();
                            }
                        }
                    }
                });
    }

    private void requestTimer() {
        getView().hideCodeResend();
        final int[] duration = {30};
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                duration[0]--;
                ((Activity) getView()).runOnUiThread(new TimerTask() {
                    @Override
                    public void run() {
                        getView().updateCounter(duration[0]);
                        if (duration[0] == 0) {
                            timer.cancel();
                            getView().showCodeResend();
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    private void getUserID(String id) {
        APIManager.getInstance().getAPI().getStudent(id).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                getView().hideProgress();
                Student student = response.body();
                if (student.getFirstName() == null)
                    student.setFirstName("");
                if (student.getLastName() == null)
                    student.setLastName("");
                BaseApplication.editor.putInt("userID", student.getId());
                BaseApplication.editor.putInt("points", student.getPoints());
                BaseApplication.editor.putString("idmUuid", student.getIdmUuid());
                BaseApplication.editor.putString("name", student.getFullName());
                BaseApplication.editor.putBoolean("isLogged", true);
                BaseApplication.editor.putBoolean("isComplete", student.isComplete());
                if (student.getPictureUrl() != null)
                    BaseApplication.editor.putString("photo", student.getPictureUrl());
                else
                    BaseApplication.editor.putString("photo", "");

                BaseApplication.editor.commit();
                getView().openApplication();
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                getView().hideProgress();
            }
        });
    }
}
