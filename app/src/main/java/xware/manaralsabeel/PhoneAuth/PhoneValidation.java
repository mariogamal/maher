package xware.manaralsabeel.PhoneAuth;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import xware.manaralsabeel.R;

public class PhoneValidation extends AppCompatActivity {

    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_validation);
//        String prefix = getCountryCode().equals(getString(R.string.egypt_code))
//            ? "" : getString(R.string.phone_prefix);

        phone = findViewById(R.id.phone);
        //phone.setText(prefix);
        Selection.setSelection(phone.getText(), phone.getText().length());

//        phone.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable text) {
//                if (!text.toString().startsWith(prefix)) {
//                    phone.setText(prefix);
//                    Selection.setSelection(phone.getText(), phone.getText().length());
//                }
//
//            }
//        });
    }

    public void register(View view) {
        Intent intent = new Intent(this, CodeValidation.class);
        intent.putExtra("phone", phone.getText().toString());
        startActivity(intent);
        finish();
    }

    private String getCountryCode() {
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getNetworkCountryIso();
    }
}
