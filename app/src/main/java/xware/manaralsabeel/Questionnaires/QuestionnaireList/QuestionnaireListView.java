package xware.manaralsabeel.Questionnaires.QuestionnaireList;

import java.util.List;

import xware.manaralsabeel.Base.BaseView;

public interface QuestionnaireListView extends BaseView {
    void displayQuestionnaires(List<Questionnaire> questionnaires);
}
