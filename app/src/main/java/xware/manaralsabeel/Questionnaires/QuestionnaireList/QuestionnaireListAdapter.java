package xware.manaralsabeel.Questionnaires.QuestionnaireList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import xware.manaralsabeel.R;

public class QuestionnaireListAdapter extends BaseAdapter {

    private List<Questionnaire> questionnaires;
    private Context context;

    class ViewHolder{
        TextView title;
    }

    QuestionnaireListAdapter(List<Questionnaire> questionnaires, Context context) {
        this.questionnaires = questionnaires;
        this.context = context;
    }

    @Override
    public int getCount() {
        return questionnaires.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.questionnaire_item_layout, null);
            holder.title = convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();
        holder.title.setText(questionnaires.get(index).getTitle());
        return convertView;
    }

}
