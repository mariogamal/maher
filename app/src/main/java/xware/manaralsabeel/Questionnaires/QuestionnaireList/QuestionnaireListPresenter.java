package xware.manaralsabeel.Questionnaires.QuestionnaireList;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class QuestionnaireListPresenter extends BasePresenter<QuestionnaireListView> {

    public void getQuestionnaires(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getQuestionnaires().enqueue(new Callback<List<Questionnaire>>() {
            @Override
            public void onResponse(Call<List<Questionnaire>> call, Response<List<Questionnaire>> response) {
                getView().displayQuestionnaires(getPublished(response.body()));
            }

            @Override
            public void onFailure(Call<List<Questionnaire>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
            }
        });
    }

    private List<Questionnaire> getPublished(List<Questionnaire> questionnaires) {
        List<Questionnaire> publishedQuestionnaires = new ArrayList<>();
        for (Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getPublished())
                publishedQuestionnaires.add(questionnaire);
        }
        return publishedQuestionnaires;
    }

}
