package xware.manaralsabeel.Questionnaires.QuestionnaireList;

import android.webkit.URLUtil;

import com.google.gson.annotations.SerializedName;

public class Questionnaire {
    @SerializedName("id")
    private Integer id;

    @SerializedName("isActive")
    private Boolean isActive;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    @SerializedName("isPublished")
    private Boolean isPublished;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        if (URLUtil.isValidUrl(url))
            return url;
        else
            return null;
    }

    public void setUrl(String url) {
        if (URLUtil.isValidUrl(url))
            this.url = url;
    }

    public Boolean getPublished() {
        return isPublished;
    }

    public void setPublished(Boolean published) {
        isPublished = published;
    }
}
