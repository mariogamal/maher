package xware.manaralsabeel.Questionnaires.QuestionnaireList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.Questionnaires.QuestionnaireForm.QuestionnaireFormFragment;
import xware.manaralsabeel.R;

public class QuestionnaireListFragment extends BaseFragment implements QuestionnaireListView {

    ListView questionnairesList;
    QuestionnaireListPresenter presenter;
    List<Questionnaire> questionnaires;
    public QuestionnaireListFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questionnaire_list, container, false);

        questionnairesList = view.findViewById(R.id.questionnairesList);
        questionnairesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                BaseApplication.questionnaire = questionnaires.get(index);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content, new QuestionnaireFormFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        presenter = new QuestionnaireListPresenter();
        presenter.attachView(this);
        presenter.getQuestionnaires();

        return view;
    }

    @Override
    public void displayQuestionnaires(List<Questionnaire> questionnaires) {
        this.questionnaires = questionnaires;
        QuestionnaireListAdapter adapter = new QuestionnaireListAdapter(questionnaires, getActivity());
        questionnairesList.setAdapter(adapter);
        hideProgress();
    }
}
