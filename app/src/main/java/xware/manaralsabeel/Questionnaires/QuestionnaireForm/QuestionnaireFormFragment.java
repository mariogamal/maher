package xware.manaralsabeel.Questionnaires.QuestionnaireForm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import io.fabric.sdk.android.services.common.SafeToast;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.R;

public class QuestionnaireFormFragment extends BaseFragment {

    public QuestionnaireFormFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questionnaire_form, container, false);
        WebView webView = view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgress();
            }
        });
        showProgress();
        webView.loadUrl(BaseApplication.questionnaire.getUrl());
        return view;
    }

}
