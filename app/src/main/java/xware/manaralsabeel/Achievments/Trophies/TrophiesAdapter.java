package xware.manaralsabeel.Achievments.Trophies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import xware.manaralsabeel.R;

public class TrophiesAdapter extends BaseAdapter {

    private List<Trophies> trophies;
    private Context context;

    public TrophiesAdapter(List<Trophies> trophies, Context context) {
        this.trophies = trophies;
        this.context = context;
    }

    @Override
    public int getCount() {
        return trophies.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.badge_item_layout,null);
            holder.fadeView = convertView.findViewById(R.id.fadeView);
            holder.icon = convertView.findViewById(R.id.icon);
            holder.title = convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder)convertView.getTag();

        holder.title.setText(trophies.get(position).getName());
        Picasso.get().load(trophies.get(position).getIconUrl())
                .placeholder(R.drawable.trophycup)
                .error(R.drawable.trophycup)
                .into(holder.icon);
        if (trophies.get(position).isHasTrophy())
            holder.fadeView.setVisibility(View.GONE);
        else
            holder.fadeView.setVisibility(View.VISIBLE);

        return convertView;
    }

    class ViewHolder{
        View fadeView;
        ImageView icon;
        TextView title;
    }
}
