package xware.manaralsabeel.Achievments.Trophies;

import com.google.gson.annotations.SerializedName;

public class Trophies {

    @SerializedName("id")
    private Integer id;

    @SerializedName("points")
    private Integer points;

    @SerializedName("name")
    private String name;

    @SerializedName("iconUrl")
    private String iconUrl;

    private boolean hasTrophy = true;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public boolean isHasTrophy() {
        return hasTrophy;
    }

    public void setHasTrophy(boolean hasTrophy) {
        this.hasTrophy = hasTrophy;
    }
}
