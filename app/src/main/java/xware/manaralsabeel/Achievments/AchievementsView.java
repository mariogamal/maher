package xware.manaralsabeel.Achievments;

import java.util.List;

import xware.manaralsabeel.Achievments.Rewards.StudentRewards;
import xware.manaralsabeel.Achievments.Trophies.Trophies;
import xware.manaralsabeel.Base.BaseView;

public interface AchievementsView extends BaseView {

    void loadTrophies(List<Trophies> trophies);

    void loadRewards(List<StudentRewards> rewards);
}
