package xware.manaralsabeel.Achievments.Rewards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import xware.manaralsabeel.R;

public class RewardsAdapter extends BaseAdapter {

    Context context;
    List<StudentRewards> studentRewards;

    public RewardsAdapter(Context context, List<StudentRewards> studentRewards) {
        this.context = context;
        this.studentRewards = studentRewards;
    }

    @Override
    public int getCount() {
        return studentRewards.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.reward_item_layout,null);
            holder = new ViewHolder();
            holder.title = convertView.findViewById(R.id.title);
            holder.state = convertView.findViewById(R.id.state);
            holder.icon = convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        }
        else
             holder = (ViewHolder)convertView.getTag();

        Picasso.get().load(studentRewards.get(position).getReward().getPictureUrl()).into(holder.icon);
        holder.title.setText(studentRewards.get(position).getReward().getName());
        if (studentRewards.get(position).getReceived()) {

            holder.state.setText("تم الاستلام");
            holder.state.setTextColor(context.getResources().getColor(R.color.cerulean));
        }
        else {
            holder.state.setText("متاحة للاستلام");
            holder.state.setTextColor(context.getResources().getColor(R.color.sap_green));
        }

        return convertView;
    }

    class ViewHolder{
        TextView title;
        ImageView icon;
        TextView state;
    }
}
