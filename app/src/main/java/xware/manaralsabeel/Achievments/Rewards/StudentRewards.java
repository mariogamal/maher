package xware.manaralsabeel.Achievments.Rewards;

import com.google.gson.annotations.SerializedName;

public class StudentRewards {

    @SerializedName("id")
    private Integer id;

    @SerializedName("isReceived")
    private Boolean isReceived;

    @SerializedName("reward")
    private Rewards reward;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getReceived() {
        return isReceived;
    }

    public void setReceived(Boolean received) {
        isReceived = received;
    }

    public Rewards getReward() {
        return reward;
    }

    public void setReward(Rewards reward) {
        this.reward = reward;
    }
}
