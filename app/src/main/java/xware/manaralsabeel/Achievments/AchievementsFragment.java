package xware.manaralsabeel.Achievments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import xware.manaralsabeel.Achievments.Rewards.RewardsAdapter;
import xware.manaralsabeel.Achievments.Rewards.StudentRewards;
import xware.manaralsabeel.Achievments.Trophies.Trophies;
import xware.manaralsabeel.Achievments.Trophies.TrophiesAdapter;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class AchievementsFragment extends BaseFragment implements AchievementsView {

    AchievementsPresenter presenter;
    GridView trophiesGrid;
    ListView rewardsList;
    TextView noRewards;

    public AchievementsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_achievment, container, false);
        ((NavMenu) getActivity()).setTitleText(getResources().getString(R.string.achievments));
        trophiesGrid = v.findViewById(R.id.trophiesGrid);
        rewardsList = v.findViewById(R.id.rewardsList);
        noRewards = v.findViewById(R.id.rewards);
        presenter = new AchievementsPresenter();
        presenter.attachView(this);
        presenter.getAllTrophies();
        presenter.getRewards();
        return v;
    }

    @Override
    public void loadTrophies(List<Trophies> trophies) {
        TrophiesAdapter adapter = new TrophiesAdapter(trophies, getActivity());
        trophiesGrid.setAdapter(adapter);
        hideProgress();
    }

    @Override
    public void loadRewards(List<StudentRewards> rewards) {
        if (rewards.size() == 0) {
            noRewards.setVisibility(View.VISIBLE);
            rewardsList.setVisibility(View.INVISIBLE);
        } else {
            noRewards.setVisibility(View.INVISIBLE);
            rewardsList.setVisibility(View.VISIBLE);
            RewardsAdapter adapter = new RewardsAdapter(getActivity(), rewards);
            rewardsList.setAdapter(adapter);
        }
        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
