package xware.manaralsabeel.Achievments;

import android.util.Log;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Achievments.Rewards.StudentRewards;
import xware.manaralsabeel.Achievments.Trophies.Trophies;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class AchievementsPresenter extends BasePresenter<AchievementsView> {
    public void getAllTrophies(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getAllTrophies().enqueue(new Callback<List<Trophies>>() {
            @Override
            public void onResponse(Call<List<Trophies>> call, Response<List<Trophies>> response) {
                getMyTrophies(response.body());
            }

            @Override
            public void onFailure(Call<List<Trophies>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    private void getMyTrophies(final List<Trophies> allTrophies){
        APIManager.getInstance().getAPI().getStudentTrophies(BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<List<Trophies>>() {
            @Override
            public void onResponse(Call<List<Trophies>> call, Response<List<Trophies>> response) {
                getView().loadTrophies(prepareFinalList(allTrophies,response.body()));
            }

            @Override
            public void onFailure(Call<List<Trophies>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

    private List<Trophies> prepareFinalList(List<Trophies> allTrophies,List<Trophies> myTrophies){
        for (Trophies trophy : allTrophies){
            if(!hasTrophy(myTrophies,trophy))
            {
                trophy.setHasTrophy(false);
                myTrophies.add(trophy);
            }
        }
        return myTrophies;
    }

    private boolean hasTrophy(List<Trophies> myTrophies , Trophies trophy) {
        for (Trophies tempTrophy : myTrophies){
            if (tempTrophy.getId().equals(trophy.getId()))
                return true;
        }
        return false;
    }

    public void getRewards(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getRewards(BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<List<StudentRewards>>() {
            @Override
            public void onResponse(Call<List<StudentRewards>> call, Response<List<StudentRewards>> response) {
                getView().loadRewards(response.body());
            }

            @Override
            public void onFailure(Call<List<StudentRewards>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }
}
