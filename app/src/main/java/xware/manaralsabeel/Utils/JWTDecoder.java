package xware.manaralsabeel.Utils;

import android.util.Base64;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JWTDecoder {

    public static String decoded(String JWTEncoded) {
        String[] split = JWTEncoded.split("\\.");
        return getSub(split[1]);
    }

    private static String getSub(String strEncoded) {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        try {
            String payload = new String(decodedBytes, "UTF-8");
            JSONObject object = new JSONObject(payload);
            return object.getString("sub");
        }
        catch (Exception ignore){
            return "";
        }
    }
}