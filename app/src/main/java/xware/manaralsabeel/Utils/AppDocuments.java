package xware.manaralsabeel.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.github.barteksc.pdfviewer.PDFView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.R;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class AppDocuments {

    String fileURL, fileName;
    Context context;
    BaseFragment fragment;
    private PDFView pdfView;
    private ProgressBar progressBar;

    public AppDocuments(Context context, BaseFragment fragment, String fileName, String fileURL) {
        this.context = context;
        this.fragment = fragment;
        this.fileName = fileName;
        this.fileURL = fileURL;
    }

    public View getDocumentView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_app_document, container, false);
        pdfView = view.findViewById(R.id.pdf_view);
        progressBar = view.findViewById(R.id.progressBar);
        if (fileURL != null)
            initPDF();
        else
            pdfView.setVisibility(View.GONE);
        return view;
    }

    public void initPDF() {
        if (checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            fragment.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        else
            downloadAndView();
    }

    public void downloadAndView() {
        File maherDirectory = new File(Environment.getExternalStorageDirectory().getPath().concat("/Maher/"));
        maherDirectory.mkdirs();
        File aboutFile = new File(maherDirectory, fileName);
        if (aboutFile.exists())
            pdfView.fromFile(aboutFile).load();
        else {
            progressBar.setVisibility(View.VISIBLE);
            Ion.with(context)
                .load(fileURL)
                .progressBar(progressBar)
                .write(new File(maherDirectory, fileName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {
                        progressBar.setVisibility(View.GONE);
                        if (result != null)
                            pdfView.fromFile(result).load();
                        else
                            fragment.showMessage(BaseApplication.error_msg);
                    }
                });
        }
    }
}
