package xware.manaralsabeel.Utils.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;

import com.squareup.picasso.Picasso;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.TouchImageView;

public class ImageZoomDialog {

    private AlertDialog dialog;
    private static ImageZoomDialog mInstance;
    private TouchImageView touchImageView;
    private View dialogView;

    public static ImageZoomDialog getInstance() {
        if (mInstance == null) {
            mInstance = new ImageZoomDialog();
        }
        return mInstance;
    }

    public void show(Context context, String imageURL) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new AlertDialog.Builder(context).create();
        dialogView = LayoutInflater.from(context).inflate(R.layout.image_zoom_dialog, null);
        touchImageView = dialogView.findViewById(R.id.zoomImage);
        Picasso.get().load(imageURL).into(touchImageView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setView(dialogView);
        dialog.show();
    }

}
