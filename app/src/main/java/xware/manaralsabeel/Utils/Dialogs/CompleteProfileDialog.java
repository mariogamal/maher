package xware.manaralsabeel.Utils.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.R;

public class CompleteProfileDialog {

    private AlertDialog dialog;
    private static CompleteProfileDialog mInstance;
    private Button registerBtn, cancelBtn;
    private View alertView;

    public static CompleteProfileDialog getInstance() {
        if (mInstance == null)
            mInstance = new CompleteProfileDialog();
        return mInstance;
    }

    public void show(Context context, SubjectFragment fragment) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new AlertDialog.Builder(context).create();
        alertView = LayoutInflater.from(context).inflate(R.layout.complete_profile_dialog, null);
        registerBtn = alertView.findViewById(R.id.complete);
        cancelBtn = alertView.findViewById(R.id.cancel);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    fragment.openProfilePage();
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }

}
