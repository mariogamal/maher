package xware.manaralsabeel.Utils.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.R;

public class ExamResultDialog {

    private AlertDialog dialog;
    private static ExamResultDialog mInstance;
    private Button cancelBtn;
    private TextView resultText;
    private View alertView;

    public static ExamResultDialog getInstance() {
        if (mInstance == null)
            mInstance = new ExamResultDialog();
        return mInstance;
    }

    public void show(Activity activity, int result, int total) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new AlertDialog.Builder(activity).create();
        alertView = LayoutInflater.from(activity).inflate(R.layout.exam_result_dialog, null);
        resultText = alertView.findViewById(R.id.result_text);
        String degree = String.valueOf(result).concat("/").concat(String.valueOf(total));
        resultText.setText(activity.getResources().getString(R.string.your_degree).concat(degree));
        cancelBtn = alertView.findViewById(R.id.cancel);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                activity.onBackPressed();
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }

}
