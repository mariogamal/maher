package xware.manaralsabeel.Utils.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.R;

public class RegisterBlockDialog {

    private AlertDialog dialog;
    private static RegisterBlockDialog mInstance;
    private Button registerBtn, cancelBtn;
    private View alertView;

    public static RegisterBlockDialog getInstance() {
        if (mInstance == null)
            mInstance = new RegisterBlockDialog();
        return mInstance;
    }

    public void show(Context context, SubjectFragment fragment) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new AlertDialog.Builder(context).create();
        alertView = LayoutInflater.from(context).inflate(R.layout.subject_register_dialog, null);
        registerBtn = alertView.findViewById(R.id.register);
        cancelBtn = alertView.findViewById(R.id.cancel);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BaseApplication.preferences.getBoolean("isComplete", false))
                    fragment.registerInSubject();
                else
                    CompleteProfileDialog.getInstance().show(context, fragment);
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }

}
