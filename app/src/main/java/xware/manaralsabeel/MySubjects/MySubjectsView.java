package xware.manaralsabeel.MySubjects;

import java.util.List;
import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.BrowseSubjects.Subject;

public interface MySubjectsView extends BaseView {

    void displaySubjects(List<Subject> body);
}
