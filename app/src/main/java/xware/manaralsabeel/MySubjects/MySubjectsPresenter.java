package xware.manaralsabeel.MySubjects;

import android.util.Log;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class MySubjectsPresenter extends BasePresenter<MySubjectsView> {

    public void loadMySubjects(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getMySubjects(
                BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {
                getView().displaySubjects(response.body());
            }

            @Override
            public void onFailure(Call<List<Subject>> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());
            }
        });
    }

}
