package xware.manaralsabeel.MySubjects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.R;

public class SubjectsAdapter extends BaseAdapter {

    private List<Subject> subjects;
    private Context context;

    public SubjectsAdapter(List<Subject> subjects, Context context) {
        this.subjects = subjects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.mysubject_item_layout,null);
            holder = new ViewHolder();
            holder.icon = convertView.findViewById(R.id.icon);
            holder.level = convertView.findViewById(R.id.levelGrade);
            holder.title = convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        holder.title.setText(subjects.get(position).getName());
        Picasso.get().load(subjects.get(position).getIconUrl()).into(holder.icon);
        holder.level.setText(
                subjects.get(position).getGrade().getLevel().getName()+" - "
                +subjects.get(position).getGrade().getName());
        return convertView;
    }

    class ViewHolder{
        TextView title;
        TextView level;
        ImageView icon;
    }
}
