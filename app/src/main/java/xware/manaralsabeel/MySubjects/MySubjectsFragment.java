package xware.manaralsabeel.MySubjects;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class MySubjectsFragment extends BaseFragment implements MySubjectsView {

    MySubjectsPresenter mySubjectsPresenter;
    ListView subjectsList;
    public MySubjectsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((NavMenu) getActivity()).setTitleText(getResources().getString(R.string.subjects));
        View v = inflater.inflate(R.layout.fragment_my_subjects, container, false);
        subjectsList = v.findViewById(R.id.subjectList);
        mySubjectsPresenter = new MySubjectsPresenter();
        mySubjectsPresenter.attachView(this);
        mySubjectsPresenter.loadMySubjects();
        return v;
    }

    @Override
    public void displaySubjects(final List<Subject> subjects) {
        SubjectsAdapter adapter = new SubjectsAdapter(subjects,getActivity());
        subjectsList.setAdapter(adapter);
        hideProgress();
        subjectsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BaseApplication.levelID = subjects.get(position).getGrade().getLevel().getId();
                BaseApplication.gradeID = subjects.get(position).getGrade().getId();
                BaseApplication.subjectID = subjects.get(position).getId();
                BaseApplication.subject = subjects.get(position);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content,new SubjectFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mySubjectsPresenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }

}
