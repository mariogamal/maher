package xware.manaralsabeel.MySubjects;

import com.google.gson.annotations.SerializedName;

public class MySubject {

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("id")
    private Integer id;

    @SerializedName("grade")
    private Grade grade;

    @SerializedName("name")
    private String name;

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
