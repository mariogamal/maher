package xware.manaralsabeel.DocumentsAgreement;

import xware.manaralsabeel.Base.BaseView;
import xware.manaralsabeel.ContactUs.Contact;

public interface DocumentsAgeementView extends BaseView {
    void bindDocumentsLinks(Contact contact);
}
