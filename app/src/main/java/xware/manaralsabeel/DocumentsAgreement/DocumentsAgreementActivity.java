package xware.manaralsabeel.DocumentsAgreement;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import xware.manaralsabeel.Base.BaseActivity;
import xware.manaralsabeel.ContactUs.Contact;
import xware.manaralsabeel.NavMenu;
import xware.manaralsabeel.R;

public class DocumentsAgreementActivity extends BaseActivity implements DocumentsAgeementView {

    String privacyURL, conditionsURL;
    DocumentsAgreementPresenter presenter;
    CheckBox privacyPolicy, termsConditions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents_agreement);
        privacyPolicy = findViewById(R.id.privacy_policy);
        termsConditions = findViewById(R.id.terms_condition);
        presenter = new DocumentsAgreementPresenter();
        presenter.attachView(this);
        presenter.getDocuments();
    }

    public void checkAgreement(View view) {
        if (termsConditions.isChecked() && privacyPolicy.isChecked())
            start(NavMenu.class);
        else
            showMessage(getString(R.string.accept_before_continue));
    }

    @Override
    public void bindDocumentsLinks(Contact contact) {
        privacyURL = contact.getPrivacyPolicy();
        conditionsURL = contact.getConditions();
        if(privacyPolicy == null || conditionsURL == null)
            start(NavMenu.class);
    }

    public void openDocument(View view) {
        String url;
        if (view.getTag().toString().equals("privacyPolicy"))
            url = privacyURL;
        else
            url = conditionsURL;
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setData(Uri.parse(url));
        startActivity(pdfIntent);
    }
}
