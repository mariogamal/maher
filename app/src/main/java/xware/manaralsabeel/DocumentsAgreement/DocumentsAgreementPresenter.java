package xware.manaralsabeel.DocumentsAgreement;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.ContactUs.Contact;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class DocumentsAgreementPresenter extends BasePresenter<DocumentsAgeementView> {

    public void getDocuments() {
        getView().showProgress();
        APIManager.getInstance().getAPI().getContactDetails().enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                getView().bindDocumentsLinks(response.body());
                getView().hideProgress();
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
            }
        });
    }
}
