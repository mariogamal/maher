package xware.manaralsabeel.Base;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;
import xware.manaralsabeel.Profile.Student;

public class BasePresenter<T extends BaseView> {
    private T view;

    public void attachView(T view){
        this.view = view;
    }

    public void detachView(){
        this.view = null;
    }

    public T getView(){
        return view;
    }

    public void getStudentData(){
        APIManager.getInstance().getAPI().getStudent(
                BaseApplication.preferences.getString("id",""))
                .enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {
                        if (response.code()==200)
                            updateStudentData(response.body());
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {
                    }
                });
    }

    private void updateStudentData(Student student){
        if (student.getFirstName() == null)
            student.setFirstName("");
        if (student.getLastName() == null)
            student.setLastName("");
        BaseApplication.editor.putInt("points", student.getPoints());
        BaseApplication.editor.putString("name", student.getFullName());
        if (student.getPictureUrl()!=null)
            BaseApplication.editor.putString("photo", student.getPictureUrl());
        else
            BaseApplication.editor.putString("photo", "");

        BaseApplication.editor.commit();
    }
}
