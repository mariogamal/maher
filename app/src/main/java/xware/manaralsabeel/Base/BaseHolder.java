package xware.manaralsabeel.Base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public abstract class BaseHolder {

    public View convertView;

    public BaseHolder(View convertView) {
        this.convertView = convertView;
    }

    public static BaseHolder constructHolder(Context context, View convertView, BaseHolder holder, int resourceID, int index){
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(resourceID, null);
            holder.convertView = convertView;
            holder.initViews();
            holder.convertView.setTag(holder);
        } else {
            holder = (BaseHolder) convertView.getTag();
            holder.convertView = convertView;
        }
        holder.setValues(index);
        return holder;
    }

    public abstract void initViews();

    public abstract void setValues(int index);
}
