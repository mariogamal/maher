package xware.manaralsabeel.Base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.fabric.sdk.android.Fabric;

import java.util.List;
import java.util.concurrent.TimeUnit;

import xware.manaralsabeel.BrowseSubjects.Levels.Level;
import xware.manaralsabeel.BrowseSubjects.Live.LiveLessons.LiveLesson;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons.RecordedLesson;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.Teacher;
import xware.manaralsabeel.Questionnaires.QuestionnaireList.Questionnaire;
import xware.manaralsabeel.R;
import xware.manaralsabeel.Utils.FontsOverride;
import xware.manaralsabeel.Utils.PropertiesReader;

public class BaseApplication extends Application {

    public static List<Level> levels;
    public static List<Teacher> teachers;
    public static Integer levelID, gradeID, subjectID, lessonID;
    public static String TeacherID;
    public static Subject subject;
    public static RecordedLesson lesson;
    public static LiveLesson liveLesson;
    public static String startDate, startTime;
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;
    public static String error_msg;
    private RefWatcher refWatcher;
    private static Context context;
    public static boolean halfHourExceeded;
    public static boolean isMySubject;
    public static Teacher teacher;
    public static String titleTextNumber;
    public static Questionnaire questionnaire;
    public static boolean isLessonQuestions;
    public static String aboutApp, privacyPolicy, termsCondition;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        refWatcher = LeakCanary.install(this);

        Fabric.with(this, new Crashlytics());
        error_msg = context.getString(R.string.please_try_again);
        preferences = getSharedPreferences("BasePref", MODE_PRIVATE);
        editor = preferences.edit();
        long endTime = preferences.getLong("endTime", 0);
        long startTime = preferences.getLong("startTime", 0);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(endTime - startTime);
        if (minutes > 30)
            halfHourExceeded = true;
        else {
            halfHourExceeded = false;
            editor.putLong("startTime", System.currentTimeMillis()).apply();
        }

        FontsOverride.setDefaultFont(this, getString(R.string.monospace_font), getString(R.string.cairo_regular_font));
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public static RefWatcher getRefWatcher(Context context) {
        BaseApplication application = (BaseApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    public static void preventAccess() {
        editor.putBoolean("isLogged", false);
        editor.commit();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public static Context getAppContext() {
        return context;
    }
}
