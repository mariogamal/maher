package xware.manaralsabeel.Base;

public interface BaseView {
    void showProgress();
    void hideProgress();
    void showMessage(String message);
}
