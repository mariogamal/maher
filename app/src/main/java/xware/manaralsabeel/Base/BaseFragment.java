package xware.manaralsabeel.Base;

import android.support.v4.app.Fragment;
import android.widget.Toast;
import xware.manaralsabeel.Utils.Dialogs.ProgressDialog;

public class BaseFragment extends Fragment implements BaseView {

    public BaseFragment() {   }

    @Override
    public void showProgress() {
        ProgressDialog.getInstance().show(getActivity());
    }

    @Override
    public void hideProgress() {
        ProgressDialog.getInstance().dismiss();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
