package xware.manaralsabeel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import java.util.Timer;
import java.util.TimerTask;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Home.HomeActivity;

public class Splash extends AppCompatActivity {

    public static String TAG = "logResult";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(BaseApplication.preferences.getBoolean("isLogged",false))
                    startActivity(new Intent(Splash.this, NavMenu.class));
                else
                    startActivity(new Intent(Splash.this, HomeActivity.class));
                finish();
            }
        },2500);
    }
}
