package xware.manaralsabeel.NetworkAPI.Retrofit;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import xware.manaralsabeel.Achievments.Rewards.StudentRewards;
import xware.manaralsabeel.Achievments.Trophies.Trophies;
import xware.manaralsabeel.BrowseSubjects.Levels.Grade;
import xware.manaralsabeel.BrowseSubjects.Levels.Level;
import xware.manaralsabeel.BrowseSubjects.Live.LiveDetails.LessonAttachments.Attachment;
import xware.manaralsabeel.BrowseSubjects.Live.LiveLessons.LiveLesson;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.Comments.Comment;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedLessons.RecordedLesson;
import xware.manaralsabeel.BrowseSubjects.ExamQuestions.Question;
import xware.manaralsabeel.BrowseSubjects.Subject;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.Rate;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.Teacher;
import xware.manaralsabeel.ContactUs.Contact;
import xware.manaralsabeel.ContactUs.Message;
import xware.manaralsabeel.Conversations.ChatMessages.ChatResponse;
import xware.manaralsabeel.Notifications.NotificationPage.Notification;
import xware.manaralsabeel.Notifications.NotificationPage.NotificationResponse;
import xware.manaralsabeel.PhoneAuth.Authorization;
import xware.manaralsabeel.Profile.Student;
import xware.manaralsabeel.Questionnaires.QuestionnaireList.Questionnaire;

public interface APIFunctions {

    @POST("/api/v1/students/")
    public Call<Authorization> authStudent(@Body HashMap<String, String> credentials);

    @PUT("/api/v1/students/{id}/")
    public Call<Student> updateStudent(@Path("id") String id, @Body Student student);

    @GET("/api/v1/students/{id}/")
    public Call<Student> getStudent(@Path("id") String id);

    @GET("/api/v1/levels/")
    public Call<List<Level>> getLevels();

    @GET("/api/v1/levels/{lpk}/grades/")
    public Call<List<Grade>> getLevelGrades(@Path("lpk") Integer levelID);

    @GET("/api/v1/grades/{gpk}/subjects/")
    public Call<List<Subject>> getGradeSubjects(@Path("gpk") Integer gradeID);

    @GET("/api/v1/subjects/{spk}/teachers/")
    public Call<List<Teacher>> getSubjectTeachers(@Path("spk") Integer subjectID);

    @GET("/api/v1/subjects/{spk}/teachers/{tpk}/lessons/recorded/")
    public Call<List<RecordedLesson>> getRecordedLessons(@Path("spk") Integer subjectID,
                                                         @Path("tpk") Integer teacherID);

    @GET("/api/v1/subjects/{spk}/lessons/live/")
    public Call<List<LiveLesson>> getLiveLessons(@Path("spk") Integer subjectID);

    @POST("/api/v1/subjects/{spk}/students/")
    public Call<HashMap> registerSubject(@Path("spk") Integer subjectID,
                                         @Body HashMap<String, Integer> studentID);

    @DELETE("/api/v1/subjects/{spk}/students/{stpk}/")
    public Call<HashMap> deleteSubject(@Path("spk") Integer subjectID,
                                       @Path("stpk") String studentID);

    @GET("/api/v1/students/{student_pk}/subjects/")
    public Call<List<Subject>> getMySubjects(@Path("student_pk") String studentID);

    @POST("/api/v1/contacts/messages/")
    public Call<Message> sendContactMsg(@Body HashMap<String, String> message);

    @GET("/api/v1/students/{student_pk}/trophies/")
    public Call<List<Trophies>> getStudentTrophies(@Path("student_pk") String studentID);

    @GET("/api/v1/trophies/")
    public Call<List<Trophies>> getAllTrophies();

    @GET("/api/v1/students/{student_pk}/rewards/")
    public Call<List<StudentRewards>> getRewards(@Path("student_pk") String studentID);

    @GET("/api/v1/lessons/{lesson_pk}/files/")
    public Call<List<Attachment>> getLessonAttachments(@Path("lesson_pk") Integer lessonID);

    @GET("/api/v1/subjects/{spk}/students/{stpk}/")
    public Call<Subject> checkSubjectExistence(@Path("spk") Integer subjectID,
                                               @Path("stpk") String studentID);

    @POST("/api/v1/lessons/{lesson_pk}/comments/")
    public Call<Comment> postStudentComment(@Path("lesson_pk") Integer lessonID,
                                            @Body HashMap<String, String> comment);

    @GET("/api/v1/lessons/{lesson_pk}/comments/")
    public Call<List<Comment>> getLessonComments(@Path("lesson_pk") Integer lessonID);

    @GET("/api/v1/lessons/recorded/{lesson_pk}/questions/")
    public Call<List<Question>> getLessonQuestions(@Path("lesson_pk") Integer lessonID);

    @GET("/api/v1/subjects/{spk}/questions/")
    public Call<List<Question>> getSubjectQuestions(@Path("spk") Integer subjectID);

    @GET("/api/v1/contacts/1/")
    public Call<Contact> getContactDetails();

    @Multipart
    @POST("/api/v1/file/rad")
    public Call<HashMap<String, String>> postImage(@Part MultipartBody.Part image);

    @GET("/api/v1/teachers/{uuid}/rate/")
    public Call<Rate> getTeacherRate(@Path("uuid") String teacherUID);

    @POST("/api/v1/teachers/{uuid}/rate/")
    public Call<Rate> sendTeacherRate(@Path("uuid") String teacherUID,
                                      @Body HashMap<String, Float> rate);

    @POST("/api/v1/points/")
    public Call<Student> recordAchievement(@Body HashMap<String, String> identifier);

    @GET("/api/v1/notifications/")
    public Call<NotificationResponse> getNotifications(@Query("page") Integer pageNumber);

    @PATCH("/api/v1/notifications/{id}/")
    public Call<Notification> markNotificationAsSeen(@Path("id") Integer notificationID,
                                                     @Body HashMap<String, Boolean> isSeen);

    @GET("/api/v1/questionnaires/")
    public Call<List<Questionnaire>> getQuestionnaires();

    @GET("/api/v1/chats/messages/{student_uuid}/{teacher_uuid}/")
    public Call<ChatResponse> getChatMessages(@Path("student_uuid") String studentUID,
                                              @Path("teacher_uuid") String teacherUID,
                                              @Query("page") Integer pageNumber);

}
