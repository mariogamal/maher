package xware.manaralsabeel.NetworkAPI.Retrofit;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BuildConfig;
import xware.manaralsabeel.PhoneAuth.Authorization;
import xware.manaralsabeel.Utils.JWTDecoder;

public class APIManager {

    private static APIManager mInstance;
    private static final String BASE_URL = BuildConfig.baseUrl;
    private Retrofit mRetrofit;

    private APIManager(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Interceptor headerAuth = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                okhttp3.Request request = chain.request();
                if (request.url().toString().equals(BASE_URL.concat("/api/v1/students/")))
                    return chain.proceed(request);

                Headers headers = request.headers().newBuilder().add("Authorization","JWT "+
                        BaseApplication.preferences.getString("Authorization","")).build();
                request = request.newBuilder().headers(headers).build();

                Response response = chain.proceed(request);
                switch (response.code()){
                    case 403:
                        BaseApplication.preventAccess();
                        break;

                    case 401:
                        HashMap<String, String> credentials = new HashMap<>();
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        credentials.put("phone", user.getPhoneNumber());
                        credentials.put("password",user.getUid());
                        credentials.put("device_token", FirebaseInstanceId.getInstance().getToken());
                        retrofit2.Response<Authorization> loginResponse = APIManager.getInstance().getAPI().authStudent(credentials).execute();
                        if (loginResponse.isSuccessful()){
                            Authorization auth = loginResponse.body();
                            BaseApplication.editor.putString("Authorization",auth.getIdToken());
                            BaseApplication.editor.putString("id", JWTDecoder.decoded(auth.getIdToken()));
                            BaseApplication.editor.commit();
                            Request.Builder builder = request.newBuilder().header("Authorization","JWT "+auth.getIdToken())
                                    .method(request.method(), request.body());
                            response = chain.proceed(builder.build());
                        }
                        break;
                }
                return response;
            }
        };

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.readTimeout(60, TimeUnit.SECONDS);
        client.connectTimeout(60, TimeUnit.SECONDS);
        client.addInterceptor(interceptor);
        client.addInterceptor(headerAuth);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
    }

    private APIManager(String baseURl){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(interceptor);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseURl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
    }

    public static APIManager getUniqueInstance(String baseURl){
        return new APIManager(baseURl);
    }

    public static APIManager getInstance(){
        if (mInstance == null) {
            mInstance = new APIManager();
        }
        return mInstance;
    }

    public APIFunctions getAPI(){
        return mRetrofit.create(APIFunctions.class);
    }
}
