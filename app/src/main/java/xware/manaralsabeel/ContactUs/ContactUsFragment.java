package xware.manaralsabeel.ContactUs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.R;

public class ContactUsFragment extends BaseFragment implements ContactUsView {

    ContactUsPresenter presenter;
    EditText message;
    TextView number;
    ImageView facebook,twitter;
    public ContactUsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_us, container, false);
        number = v.findViewById(R.id.number);
        facebook = v.findViewById(R.id.facebookURL);
        twitter = v.findViewById(R.id.twitterURL);
        presenter = new ContactUsPresenter();
        presenter.attachView(this);
        presenter.getContact();
        Button send = v.findViewById(R.id.send);
        message = v.findViewById(R.id.message);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendMessage(message.getText().toString());
            }
        });
        return v;
    }

    @Override
    public void clearText() {
        message.setText("");
    }

    @Override
    public void displayContactDetails(final Contact contact) {
        if (contact != null){
            if (contact.getPhone()!=null)
            number.setText(contact.getPhone());

            facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(contact.getFacebook_url()));
                        startActivity(i);
                    }
                    catch (Exception ignore){}
                }
            });

            twitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(contact.getTwitter_url()));
                        startActivity(i);
                    }
                    catch (Exception ignore){}
                }
            });
        }
        else
            showMessage("لا تتوافر البيانات");

        hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        BaseApplication.getRefWatcher(getActivity()).watch(this);
    }
}
