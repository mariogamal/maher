package xware.manaralsabeel.ContactUs;

import xware.manaralsabeel.Base.BaseView;

public interface ContactUsView extends BaseView {

    void clearText();

    void displayContactDetails(Contact contact);

}
