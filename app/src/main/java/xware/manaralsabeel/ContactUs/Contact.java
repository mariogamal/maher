package xware.manaralsabeel.ContactUs;

import com.google.gson.annotations.SerializedName;

public class Contact {
    @SerializedName("phone")
    private String phone;

    @SerializedName("facebookUrl")
    private String facebookUrl;

    @SerializedName("twitterUrl")
    private String twitterUrl;

    @SerializedName("aboutApp")
    private String aboutApp;

    @SerializedName("privacyPolicy")
    private String privacyPolicy;

    @SerializedName("conditions")
    private String conditions;

    public String getAboutApp() {
        return aboutApp;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public String getConditions() {
        return conditions;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook_url() {
        return facebookUrl;
    }

    public void setFacebook_url(String facebook_url) {
        this.facebookUrl = facebook_url;
    }

    public String getTwitter_url() {
        return twitterUrl;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitterUrl = twitter_url;
    }
}
