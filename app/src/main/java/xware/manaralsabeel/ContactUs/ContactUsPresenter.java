package xware.manaralsabeel.ContactUs;

import android.util.Log;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BasePresenter;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;

public class ContactUsPresenter extends BasePresenter<ContactUsView> {

    void getContact(){
        getView().showProgress();
        APIManager.getInstance().getAPI().getContactDetails().enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                getView().displayContactDetails(response.body());
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }
    void sendMessage(final String message){
        getView().showProgress();
        HashMap<String,String> mapMessage = new HashMap<>();
        mapMessage.put("message",message);
        APIManager.getInstance().getAPI().sendContactMsg(mapMessage).enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                getView().hideProgress();
                String responseMessage = response.body().getMessage();
                if (responseMessage!=null && responseMessage.equals(message)) {
                    getView().showMessage("تم الارسال");
                    getView().clearText();
                }
                else
                    getView().showMessage("حدث خطأ");
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                getView().hideProgress();
                getView().showMessage(BaseApplication.error_msg);
                Log.d("responseError",t.toString());            }
        });
    }
}
