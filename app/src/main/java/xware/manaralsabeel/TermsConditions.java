package xware.manaralsabeel;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.Base.BaseFragment;
import xware.manaralsabeel.Utils.AppDocuments;

public class TermsConditions extends BaseFragment {

    AppDocuments appDocuments;

    public TermsConditions() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        appDocuments = new AppDocuments(getActivity(), this, "TermsAndCond.pdf", BaseApplication.termsCondition);
        return appDocuments.getDocumentView(inflater, container);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                appDocuments.downloadAndView();
            else
                showMessage(getString(R.string.storage_permission_required));
        }
    }
}
