package xware.manaralsabeel;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xware.manaralsabeel.Achievments.AchievementsFragment;
import xware.manaralsabeel.Base.BaseActivity;
import xware.manaralsabeel.Base.BaseApplication;
import xware.manaralsabeel.BrowseSubjects.LevelsPager;
import xware.manaralsabeel.BrowseSubjects.Recorded.RecordedDetails.RecordedDetailFragment;
import xware.manaralsabeel.BrowseSubjects.ViewSubject.SubjectFragment;
import xware.manaralsabeel.ContactUs.Contact;
import xware.manaralsabeel.ContactUs.ContactUsFragment;
import xware.manaralsabeel.MySubjects.MySubjectsFragment;
import xware.manaralsabeel.NetworkAPI.Retrofit.APIManager;
import xware.manaralsabeel.Notifications.NotificationPage.NotificationFragment;
import xware.manaralsabeel.Notifications.NotificationPage.NotificationResponse;
import xware.manaralsabeel.Profile.Student;
import xware.manaralsabeel.Profile.UserProfile;
import xware.manaralsabeel.Questionnaires.QuestionnaireList.QuestionnaireListFragment;
import xware.manaralsabeel.Utils.PropertiesReader;

public class NavMenu extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    TextView titleText;
    static ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    RelativeLayout notifications;
    public AppBarLayout appBarLayout;
    public YouTubePlayer youTubePlayer;
    public boolean isYoutubePlayerFullScren;
    FragmentManager fragmentManager;
    private View badgeBG;
    private TextView badgeCount;

    BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BaseApplication.editor.putInt("notifyCount",
                    BaseApplication.preferences.getInt("notifyCount", 0) + 1)
                    .commit();
            setBadgeCount();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_menu);
        registerReceiver(notificationReceiver, new IntentFilter("NEW_NOTIFICATION"));

        fragmentManager = getSupportFragmentManager();
        appBarLayout = findViewById(R.id.appBarLayout);
        badgeBG = findViewById(R.id.badgeBG);
        badgeCount = findViewById(R.id.badgeCount);
        notifications = findViewById(R.id.notifications);
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginFragmentTransaction(new NotificationFragment(), true);
                resetNotificationCount();
            }
        });
        getDocuments();
        getNotificationCount();

        if (BaseApplication.halfHourExceeded &&
                !BaseApplication.preferences.getBoolean("firstHalf", false))
            recordAchievement(R.string.STAY_IN_APP_HALF_HOUR);

        initDrawer(getIntent().getStringExtra("object") == null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBadgeCount();
    }

    public void setBadgeCount() {
        int count = BaseApplication.preferences.getInt("notifyCount", 0);
        if (count == 0) {
            badgeBG.setVisibility(View.GONE);
            badgeCount.setVisibility(View.GONE);
        } else {
            badgeBG.setVisibility(View.VISIBLE);
            badgeCount.setVisibility(View.VISIBLE);
            badgeCount.setText(String.valueOf(count));
        }
    }

    private void resetNotificationCount() {
        BaseApplication.editor.putInt("notifyCount", 0).commit();
        setBadgeCount();
    }

    public void initDrawer(boolean noNavigation) {
        Locale.setDefault(new Locale("ar"));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        titleText = findViewById(R.id.titleText);
        getSupportActionBar().setTitle("");

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                updateDrawerHeader();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                updateDrawerHeader();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (noNavigation) {
            navigationView.getMenu().getItem(1).setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(1));
        } else {
            resetNotificationCount();
            setTitleText(getResources().getString(R.string.notifications));
            beginFragmentTransaction(new NotificationFragment(), false);
        }

        updateDrawerHeader();
    }

    public void updateDrawerHeader() {
        View headerView = navigationView.getHeaderView(0);
        CircleImageView profile = headerView.findViewById(R.id.menuProfilePic);
        String photo = BaseApplication.preferences.getString("photo", "");
        if (!photo.equals(""))
            Picasso.get().load(photo)
                    .placeholder(R.drawable.profileicon)
                    .error(R.drawable.profileicon)
                    .into(profile);
        else
            profile.setImageResource(R.drawable.profileicon);

        TextView name = headerView.findViewById(R.id.userName);
        name.setText(BaseApplication.preferences.getString("name", ""));
        TextView phone = headerView.findViewById(R.id.userPhone);
        phone.setText(BaseApplication.preferences.getString("phone", ""));
        TextView points = headerView.findViewById(R.id.points);
        points.setText("x".concat(String.valueOf(BaseApplication.preferences.getInt("points", 0))));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        drawer.closeDrawers();
        switch (item.getItemId()) {
            case R.id.profile:
                beginFragmentTransaction(new UserProfile(), false);
                break;

            case R.id.learn:
                beginFragmentTransaction(new LevelsPager(), false);
                break;

            case R.id.subjects:
                beginFragmentTransaction(new MySubjectsFragment(), false);
                break;

            case R.id.achievments:
                if (BaseApplication.preferences.getBoolean("isComplete", false))
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, new AchievementsFragment()).commit();
                else
                    showMessage(getResources().getString(R.string.complete_profile_first));
                return true;

            case R.id.questionnaires:
                beginFragmentTransaction(new QuestionnaireListFragment(), false);
                break;

            case R.id.contact_Us:
                beginFragmentTransaction(new ContactUsFragment(), false);
                break;

            case R.id.share:
                shareApp();
                return true;

            case R.id.aboutApp:
                beginFragmentTransaction(new AboutApp(), false);
                break;
        }
        titleText.setText(item.getTitle());
        return true;
    }

    private void shareApp() {
        drawer.closeDrawers();
        String share_message = getResources().getString(R.string.share_msg)
                .concat(PropertiesReader.getProperty("appUrl", this));
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, share_message);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.RIGHT))
            drawer.closeDrawers();
        else if (youTubePlayer != null && isYoutubePlayerFullScren)
            youTubePlayer.setFullscreen(false);
        else if (fragmentManager.getBackStackEntryCount() > 0) {
            super.onBackPressed();
            changeTitle();
        } else {
            Fragment current = fragmentManager.findFragmentById(R.id.content);
            if (current instanceof LevelsPager)
                finish();
            else {
                titleText.setText("تصفح المواد");
                beginFragmentTransaction(new LevelsPager(), false);
            }
        }
    }

    private void changeTitle() {
        Fragment current = fragmentManager.findFragmentById(R.id.content);
        if (current instanceof SubjectFragment)
            setTitleText(getResources().getString(R.string.browse_subjects));
        else if (current instanceof NotificationFragment)
            setTitleText(getResources().getString(R.string.notifications));
        else if (current instanceof RecordedDetailFragment)
            setTitleText(getResources().getString(R.string.recorded_lessons));
        else if (current instanceof MySubjectsFragment)
            setTitleText(getResources().getString(R.string.my_subjects));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
        if (!BaseApplication.halfHourExceeded)
            BaseApplication.editor.putLong("endTime", System.currentTimeMillis()).apply();
    }

    public void recordAchievement(final int id) {
        HashMap<String, String> identifier = new HashMap<>();
        identifier.put("identifier", getString(id));
        APIManager.getInstance().getAPI().recordAchievement(identifier).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                if (response.code() == 200 && id == R.string.STAY_IN_APP_HALF_HOUR) {
                    BaseApplication.editor.putBoolean("firstHalf", true).apply();
                    BaseApplication.editor.putInt("points", response.body().points).apply();
                    updateDrawerHeader();
                }
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {

            }
        });
    }

    public void termsCondition(View view) {
        titleText.setText(R.string.terms_and_conditions);
        drawer.closeDrawers();
        beginFragmentTransaction(new TermsConditions(), false);
    }

    public void privacyPolicy(View view) {
        titleText.setText(R.string.privacy_policy);
        drawer.closeDrawers();
        beginFragmentTransaction(new PrivacyPolicy(), false);
    }

    private void beginFragmentTransaction(Fragment fragment, boolean addToBackStack) {
        if (addToBackStack)
            fragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .addToBackStack(null)
                    .commit();
        else
            fragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit();
    }

    public void setTitleText(String title) {
        titleText.setText(title);
    }

    private void getNotificationCount() {
        APIManager.getInstance().getAPI().getNotifications(null).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                BaseApplication.editor.putInt("notifyCount", response.body().getNotSeenCount()).commit();
                setBadgeCount();
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {

            }
        });
    }

    private void getDocuments(){
        APIManager.getInstance().getAPI().getContactDetails().enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                BaseApplication.aboutApp = response.body().getAboutApp();
                BaseApplication.privacyPolicy = response.body().getPrivacyPolicy();
                BaseApplication.termsCondition = response.body().getConditions();
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {

            }
        });
    }
}
